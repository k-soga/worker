/**
 * メイン-ポジション-サマリタブ テーブル描画機能
 */
const InfoPositionSummaryTableContentsDrawer = function(position) {
	this.position = position;
	this.targetCurrencyPosition = position.filter(function(result){ return this.isTargetCurrency(result) }, this);
	this.clear();
	this.draw();
}
InfoPositionSummaryTableContentsDrawer.prototype = {
	position: [],
	targetCurrencyPosition: [],

	draw: function(){
		this.viewPositionHead();
		this.viewPositionBody();
	},

	clear: function() {
		const tablePosition = document.querySelector("#tablePosition");
		if(tablePosition.querySelector("thead")) tablePosition.removeChild(tablePosition.querySelector("thead"));
		if(tablePosition.querySelector("tbody")) tablePosition.removeChild(tablePosition.querySelector("tbody"));

		const tablePaging = document.querySelector("#paging");
		if(tablePaging) document.querySelector("body").removeChild(tablePaging);
	},

	/**
	 * thead表示
	 */
	viewPositionHead: function() {
		const thead = document.createElement("thead");
		const tr = document.createElement("tr");
		tr.appendChild(this.createHeadTableData("通貨ペア", 79));
		tr.appendChild(this.createHeadTableData(this.nl2br("売\n買"), 24));
		tr.appendChild(this.createHeadTableData("最終成立日時", 110));
		tr.appendChild(this.createHeadTableData(this.nl2br("数量\n(万)"), 46));
		tr.appendChild(this.createHeadTableData("平均値", 60));
		tr.appendChild(this.createHeadTableData("現在値", 57));
		tr.appendChild(this.createHeadTableData("pips", 80));
		tr.appendChild(this.createHeadTableData("スワップ", 135));
		tr.appendChild(this.createHeadTableData("為替損益", 135));
		tr.appendChild(this.createHeadTableData(this.nl2br("評価損益\n<span class='small'>(スワップ+為替損益)</span>"), 135));
		tr.appendChild(this.createHeadTableData("決済", 105));
		thead.appendChild(tr);
		document.querySelector("#tablePosition").appendChild(thead);
	},

	/**
	 * tbody表示
	 */
	viewPositionBody: function() {
		const tbody = document.createElement("tbody");
		if(this.targetCurrencyPosition.length==0) {
			for(let i=0; i<InfoPositionSummaryTableContentsDrawer.DUMMY_ROWS; i++) {
				tbody.appendChild(this.createEmptyRow());
			}
		}else{
			this.targetCurrencyPosition.forEach(function(el) { tbody.appendChild(this.createTableRow(el)); }, this);
		}
		tbody.appendChild(this.createTotalRow());
		this.adjustTableRow(tbody);
		document.querySelector("#tablePosition").appendChild(tbody);
	},

	/**
	 * tr生成
	 */
	createTableRow: function(infoPositionSummary) {
		const tr = document.createElement("tr");
		const currencyCd = infoPositionSummary.getCurrencyCd();
		const division = infoPositionSummary.getDivision();
		tr.classList.add(currencyCd);

		//通貨ペア名称
		const currencyName = top.AppBasicDataManage.getCurrencyInfo(currencyCd).currencyName1;
		tr.appendChild(this.createBodyTableData(currencyName));

		//売買区分
		const divisionName = top.ConditionManage.getDivisionName(division);
		tr.appendChild(this.createBodyTableData(divisionName));

		//最終成立日時
		const lastDecisionDate = infoPositionSummary.getLastDecisionDate();
		const lastDecisionTime = infoPositionSummary.getLastDecisionTime();
		tr.appendChild(this.createBodyTableData(top.toDateString(lastDecisionDate) + " " + top.toTimeString(lastDecisionTime)));

		//取引数量
		tr.appendChild(this.alignRight(this.createBodyTableData(infoPositionSummary.getAmount())));

		// 平均値
		averagePrice: {
			const averagePrice = infoPositionSummary.getAveragePrice();
			const span = document.createElement("span");
			span.textContent = top.addFigure(averagePrice);
			const td = document.createElement("td");
			td.appendChild(span);
			tr.appendChild(this.alignRight(td));
		}

		//現在値
		nowPrice: {
			const nowPrice = infoPositionSummary.getNowPrice();
			const span = document.createElement("span");
			span.id = "lblNowPrice" + currencyCd + division;
			span.textContent = top.addFigure(nowPrice);
			const td = document.createElement("td");
			td.appendChild(span);
			tr.appendChild(this.alignRight(td));
		}

		//pips
		pips: {
			const exProfitPips = infoPositionSummary.getPips();
			const span = document.createElement("span");
			span.id = "lblPips"  + currencyCd + division;
			span.textContent = exProfitPips;
			const td = document.createElement("td");
			td.appendChild(span);
			if(parseFloat(exProfitPips) < 0) td.classList.add("minus");
			tr.appendChild(this.alignRight(td));
		}

		//スワップ（円）
		swap: {
			const swap = infoPositionSummary.getSwapYen();
			const span = document.createElement("span");
			span.id = "lblSwap" + currencyCd + division;
			span.textContent = top.addFigure(swap);
			const td = document.createElement("td");
			td.appendChild(span);
			td.innerHTML += infoPositionSummary.getConvertUnit();
			if(parseInt(swap) < 0) td.classList.add("minus");
			tr.appendChild(this.alignRight(td));
		}

		//為替損益（円）
		exProfit: {
			const exProfit = infoPositionSummary.getExProfitYen();
			const span = document.createElement("span");
			span.id = "lblExProfit" + currencyCd + division;
			span.textContent = top.addFigure(exProfit);
			const td = document.createElement("td");
			td.appendChild(span);
			td.innerHTML += infoPositionSummary.getConvertUnit();
			if(parseInt(exProfit) < 0) td.classList.add("minus");
			tr.appendChild(this.alignRight(td));
		}

		//評価損益（円）（スワップ+為替損益）
		profit: {
			const profit = infoPositionSummary.getConvertProfit();
			const span = document.createElement("span");
			span.id = "lblProfit" + currencyCd + division;
			span.textContent = top.addFigure(profit);
			const td = document.createElement("td");
			td.appendChild(span);
			td.innerHTML += infoPositionSummary.getConvertUnit();
			if(parseInt(profit) < 0) td.classList.add("minus");
			tr.appendChild(this.alignRight(td));
		}

		//決済
		set: {
			const td = document.createElement("td");
			const span = document.createElement("span");
			span.textContent = "一括決済";
			span.classList.add("button");
			span.classList.add("blanketOrder");
			span.onclick = function(){ onClickSetBlanketOrderButton(infoPositionSummary) }
			td.appendChild(span)
			tr.appendChild(td);
		}
		return tr;
	},

	/**
	 * 合計行(tr)作成
	 */
	createTotalRow: function() {
		const tr = document.createElement("tr");
		tr.classList.add("positionTotalTr");

		tr.appendChild(this.createEmptyTableData());
		tr.appendChild(this.createEmptyTableData());

		const totalLabel = this.createBodyTableData("合計");
		totalLabel.setAttribute("colspan", "3");
		totalLabel.style.textAlign = "left";
		tr.appendChild(totalLabel);

		tr.appendChild(this.createEmptyTableData());
		tr.appendChild(this.createEmptyTableData());
		//スワップ
		swapYen: {
			const swapYenSum = this.targetCurrencyPosition.map(function(el){
				return parseInt(el.getSwapYen())
			}).reduce(function(acc, cur){ return acc + cur }, 0);
			const span = document.createElement("span");
			span.id = "lblSwapYenSum";
			span.textContent = top.addFigure(swapYenSum);
			const td = document.createElement("td");
			td.appendChild(span);
			if(this.position.length > 0) td.innerHTML += convertSumUnit;
			if(swapYenSum < 0) td.classList.add("minus");
			tr.appendChild(this.alignRight(td));
		}

		//為替損益
		exProfitYen: {
			const exProfitSum = this.targetCurrencyPosition.map(function(el){
				return parseInt(el.getExProfitYen())
			}).reduce(function(acc, cur){ return acc + cur }, 0);
			const span = document.createElement("span");
			span.id = "lblExProfitSum";
			span.textContent = top.addFigure(exProfitSum);
			const td = document.createElement("td");
			td.appendChild(span);
			if(this.position.length > 0) td.innerHTML += convertSumUnit;
			if(exProfitSum < 0) td.classList.add("minus");
			tr.appendChild(this.alignRight(td));
		}

		//評価損益
		profitSum: {
			const profitSum = this.targetCurrencyPosition.map(function(el){
				return parseInt(el.getConvertProfit())
			}).reduce(function(acc, cur){ return acc + cur }, 0);
			const span = document.createElement("span");
			span.id = "lblProfitSum";
			span.textContent = top.addFigure(profitSum);
			const td = document.createElement("td");
			td.appendChild(span);
			if(this.targetCurrencyPosition.length > 0) td.innerHTML += convertSumUnit;
			if(profitSum < 0) td.classList.add("minus");
			tr.appendChild(this.alignRight(td));
		}

		tr.appendChild(this.createEmptyTableData());
		return tr;
	},

	createHeadTableData: function(value, width, rowspan) {
		const td = document.createElement("td");
		if(rowspan != undefined && rowspan != "") td.setAttribute("rowspan", rowspan);
		td.setAttribute("width", width);
		
		// 詳細テーブルヘッダ部の高さの差を補正（行の高さ(19px)×2）
		td.setAttribute("height", "38px");

		const div = document.createElement("div");
		div.innerHTML = value;
		div.classList.add("subtitle");
		td.appendChild(div);
		return td;
	},

	/**
	 * テーブル行 最適化
	 */
	adjustTableRow: function(tbody) {
		var nodelist = tbody.querySelectorAll("tr");
		Array.prototype.slice.call(nodelist, 0).forEach(function(tr, index){
			if(index % 2 == 0) {
				tr.classList.add("even");
				tr.classList.remove("odd");
			}else {
				tr.classList.remove("even");
				tr.classList.add("odd");
			}
		});
	},

	/**
	 * 値洗い実行
	 */
	markingToMarket: function(position) {
		const table = document.querySelector("#tablePosition");
		const targetCurrencyPosition = position.filter(function(result){ return this.isTargetCurrency(result.positionSummary) }, this);
		targetCurrencyPosition.forEach(function(result){
			const currencyCd = result.getCurrencyCd();
			const division = result.getDivision();

			//ポジションサマリ
			if(table.querySelector("#lblNowPrice" + currencyCd + division)){
				//現在値
				table.querySelector("#lblNowPrice" + currencyCd + division).textContent = result.getNowPrice();
				//スワップ（円）
				swap: {
					const el = table.querySelector("#lblSwap" + currencyCd + division);
					const swap = result.getSwapYen();
					if(swap < 0) el.parentNode.classList.add("minus");
					else el.parentNode.classList.remove("minus")
					el.textContent = top.addFigure(swap);
				}
				//pips
				pips: {
					const el = table.querySelector("#lblPips" + currencyCd + division);
					const pips = result.getPips();
					if(pips < 0) el.parentNode.classList.add("minus")
					else el.parentNode.classList.remove("minus")
					el.textContent = pips;
				}
				//為替損益（円）
				exProfit: {
					const el = table.querySelector("#lblExProfit" + currencyCd + division);
					const exProfit = result.getExProfitYen();
					if(exProfit < 0) el.parentNode.classList.add("minus");
					else el.parentNode.classList.remove("minus");
					el.textContent = top.addFigure(exProfit);
				}
				//評価損益（円）（スワップ+為替損益）
				profit: {
					const el = table.querySelector("#lblProfit" + currencyCd + division);
					const profit = result.getConvertProfit();
					if(profit < 0) el.parentNode.classList.add("minus");
					else el.parentNode.classList.remove("minus")
					el.textContent = top.addFigure(profit);
				}
			}
		})
		//評価損益合計
		this.adjustTotalProfit(targetCurrencyPosition);
	},

	/**
	 * 合計損益 最適化
	 * <pre>
	 *   通貨ペアの絞り込みに応じて、合計損益を再計算する。
	 * </pre>
	 */
	adjustTotalProfit: function(position) {
		// スワップ
		swapYenSum: {
			const swapYenSum = position.map(function(el){
				return parseInt(el.getSwapYen())
			}).reduce(function(acc, cur){ return acc + cur }, 0);
			const el = document.querySelector("#lblSwapYenSum");
			if(swapYenSum < 0) el.parentNode.classList.add("minus")
			else el.parentNode.classList.remove("minus")
			el.textContent = top.addFigure(swapYenSum);
		}

		// 為替損益
		exProfitSum: {
			const exProfitSum = position.map(function(el){
				return parseInt(el.getExProfitYen())
			}).reduce(function(acc, cur){ return acc + cur }, 0);

			const el = document.querySelector("#lblExProfitSum");
			if(exProfitSum < 0) el.parentNode.classList.add("minus")
			else el.parentNode.classList.remove("minus")
			el.textContent = top.addFigure(exProfitSum);
		}

		// 評価損益合計
		profitSum: {
			const profitSum = position.map(function(el){
				return parseInt(el.getConvertProfit())
			}).reduce(function(acc, cur){ return acc + cur }, 0);
			const el = document.querySelector("#lblProfitSum");
			if(profitSum < 0) el.parentNode.classList.add("minus")
			else el.parentNode.classList.remove("minus")
			el.textContent = top.addFigure(profitSum);
		}
	},

	createEmptyRow: function() {
		const tr = document.createElement("tr");
		for(let i=0; i<InfoPositionSummaryTableContentsDrawer.COLUMNS; i++) {
			tr.appendChild(this.createEmptyTableData());
		}
		return tr;
	},

	createEmptyTableData: function() {
		return this.createBodyTableData("&nbsp");
	},

	createBodyTableData: function(value) {
		const td = document.createElement("td");
		td.innerHTML = value;
		return td;
	},

	isTargetCurrency: function(position) {
		const selectedCurrencyCd = currencySelectBox.getSelectedCurrencyCd();
		return selectedCurrencyCd == "" || position.getCurrencyCd() == selectedCurrencyCd;
	},

	alignRight: function(td) {
		if(!isNaN(parseFloat(td.textContent))){	//右寄せは原則数値のみのため
			td.style.textAlign = "right";
			td.innerHTML += "&nbsp;";
		}
		return td;
	},

	nl2br: function(value){
		return value.replace(/\n/g, '<br>');
	}
}
InfoPositionSummaryTableContentsDrawer.DUMMY_ROWS = 10;	//レコードが無い場合のダミー表示行数
InfoPositionSummaryTableContentsDrawer.COLUMNS = 11;	//カラム数

const onClickSetBlanketOrderButton = function(infoPositionSummary) {
	if(checkAction()){
		top.openWindowSetBlanketOrder(top.SET_BLK_ODR_WIN_NO, infoPositionSummary.getCurrencyCd(), infoPositionSummary.getDivision());
	}
}