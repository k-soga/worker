import logger from '../env/Logger'
import { call, put, takeLatest } from 'redux-saga/effects';
import { initApp, onSuccessedInitBizzData } from '../store/AppTopSlice'
import { sg_getBrandData } from './BrandSaga'
import { sg_getInitRateData } from './RateSaga'
import { sg_getInitPositionData } from './PositionSaga'
import { sg_getInitEffectOrderData } from './EffectOrderSaga'

export default function* rootSaga() {
  logger.log("called rootSaga")
  yield takeLatest(initApp, sg_initApp)
}

export function* sg_initApp(action) {
  yield call(sg_initBizzData, action)
}

export function* sg_initBizzData(action) {
  logger.log('called sg_initBizzData')
  // const brandList = yield call(sg_getBrandData)
  const brandData = yield call(sg_getBrandData)
  const brandList = brandData.brandList
  const brandKeyMapList = brandData.brandKeyMapList
  const rateList = yield call(sg_getInitRateData, brandList)
  const setOrderMap2Position = yield call(sg_getInitEffectOrderData, brandList, brandKeyMapList)
  yield call(sg_getInitPositionData, brandList, brandKeyMapList, rateList, setOrderMap2Position)
  yield put(onSuccessedInitBizzData())
}

