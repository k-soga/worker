import logger from '../env/Logger'
import { put } from 'redux-saga/effects';
import BigNumber from 'bignumber.js'
import { SELL_BUY, EXEC_CONDITION, ORDER_TYPE }  from '../const/BizzConsts'
import { onGetInitPositionData } from '../store/data/PositionSlice'
import DateFormat from '../utils/DateFormat'
import { calcSubTime } from '../utils/DateUtils'
import {
  getDispOrderNo,
  getRateDigit,
  getCurrRate,
  calcPositionPips,
  calcProfit,
  getSelectBrandList
} from '../utils/BizzUtils'
import { SELECT_ALL, SELECT_LIST_CFD_ACC_TYPE } from '../const/BizzConsts'
import { _getDummyInitPositionData } from './dummy/DummyPosition'

const dateFormat = new DateFormat("yyyy/MM/dd HH:mm:ss");

export function* sg_getInitPositionData(brandList, brandKeyMapList, rateList, setOrderMap2Position) {
  logger.log('called sg_getInitPositionData')
  //ポジションリスト取得(ダミーデータ)
  let positionList = _getDummyInitPositionData()
  //値洗い
  positionList = sg_priceUpdate(brandKeyMapList, rateList, positionList)
  //ポジションリスト & 決済注文リスト生成
  const positionInSetOrderList = _createSetOrderMixList(positionList, setOrderMap2Position, brandList, brandKeyMapList)
  //ポジションサマリリスト生成
  let positionSummaryList =  _createSummaryList(positionList, brandList, brandKeyMapList)
  //CFD口座種別絞込リスト生成
  const positionSelectCdfAccList = getSelectableCdfAccList(positionList)
  //銘柄絞込リスト生成
  const positionSelectBrandList = getSelectableBrandList(brandList, positionList)

  yield put(onGetInitPositionData({
    positionList: positionList,
    positionInSetOrderList: positionInSetOrderList,
    positionSummaryList: positionSummaryList,
    positionSelectCdfAccList: positionSelectCdfAccList,
    positionSelectBrandList: positionSelectBrandList,
  }))
}

//値段洗い
export const sg_priceUpdate = (brandKeyMapList, rateList, positionList) => {
  let currRateSellBuyDiv = undefined
  let currRate = undefined
  let pips = undefined
  let profit = undefined
  //ポジションリスト 値洗い
  for(let i=0; i<positionList.length; i++) {
    //現在値 BID or OFFER 判定
    currRateSellBuyDiv = (positionList[i].sellBuyDiv !== SELL_BUY.SELL) ? SELL_BUY.SELL : SELL_BUY.BUY
    //現在値段 取得
    currRate = getCurrRate(positionList[i].brandCd, currRateSellBuyDiv, rateList, brandKeyMapList)
    //pips算出
    pips = calcPositionPips(positionList[i].contractRate, currRate, positionList[i].brandCd, brandKeyMapList)
    //評価損益算出
    profit = calcProfit(positionList[i].amount, positionList[i].contractRate, currRate)
    //結果セット
    positionList[i].currRate = currRate
    positionList[i].pips = pips
    positionList[i].profit = profit
  }

  return positionList
}

const _createSetOrderMixList = (positionList, setOrderMap2Position, brandList, brandKeyMapList) => {
  let _positionSetOrderMixList = []
  let curPositionNo = null
  let onePositionSetOrderList = null
  let _resultRecord = {}
  for(let i=0; i<positionList.length; i++) {
    curPositionNo = positionList[i].positionNo
    onePositionSetOrderList = setOrderMap2Position[curPositionNo]

    //決済注文有
    if(onePositionSetOrderList) { 
      for(let j=0; j<onePositionSetOrderList.length; j++) {
        _resultRecord = {}
        _resultRecord.amount = positionList[i].amount
        _resultRecord.brandCd = positionList[i].brandCd
        _resultRecord.cfdAccType = positionList[i].cfdAccType
        _resultRecord.contractDate = positionList[i].contractDate
        _resultRecord.contractRate = positionList[i].contractRate
        _resultRecord.positionNo = positionList[i].positionNo
        _resultRecord.currRate = positionList[i].currRate
        _resultRecord.pips = positionList[i].pips
        _resultRecord.profit = positionList[i].profit
        _resultRecord.remainingAmount = positionList[i].remainingAmount
        _resultRecord.sellBuyDiv = positionList[i].sellBuyDiv
        _resultRecord.setOrder = {}
        _resultRecord.setOrder.orderNo = onePositionSetOrderList[j].orderNo
        _resultRecord.setOrder.orderSubNo = onePositionSetOrderList[j].orderSubNo
        _resultRecord.setOrder.amount = onePositionSetOrderList[j].amount
        _resultRecord.setOrder.execCondition = onePositionSetOrderList[j].execCondition
        _resultRecord.setOrder.orderType = onePositionSetOrderList[j].orderType
        //単体正逆指(指値or逆指値)
        if(onePositionSetOrderList[j].orderType === ORDER_TYPE.SINGLE_POINT) {
          //指値
          if(onePositionSetOrderList[j].execCondition === EXEC_CONDITION.POINT) {
            _resultRecord.setOrder.pointPrice = onePositionSetOrderList[j].pointPrice
          //逆指値
          }else if(onePositionSetOrderList[j].execCondition === EXEC_CONDITION.REVERSE_POINT) {
            _resultRecord.setOrder.revPointPrice = onePositionSetOrderList[j].pointPrice
          }
        //OCO
        } else if(onePositionSetOrderList[j].orderType === ORDER_TYPE.IFD
                  || onePositionSetOrderList[j].orderType === ORDER_TYPE.IFO || onePositionSetOrderList[j].orderType === ORDER_TYPE.N_OCO 
                  || onePositionSetOrderList[j].orderType === ORDER_TYPE.L_OCO || onePositionSetOrderList[j].orderType === ORDER_TYPE.OCO_OCO) {
            //指値の場合：ペア側＝逆指値
            if(onePositionSetOrderList[j].execCondition === EXEC_CONDITION.POINT) {
              _resultRecord.setOrder.pointPrice = onePositionSetOrderList[j].pointPrice
              _resultRecord.setOrder.revPointPrice = onePositionSetOrderList[j].ocoPair.pointPrice
            //逆指値の場合：ペア側＝指値
            }else if(onePositionSetOrderList[j].execCondition === EXEC_CONDITION.REVERSE_POINT) {
              _resultRecord.setOrder.pointPrice = onePositionSetOrderList[j].ocoPair.pointPrice
              _resultRecord.setOrder.revPointPrice = onePositionSetOrderList[j].pointPrice
            }
        }
        _resultRecord._key = _resultRecord.positionNo + ':' + getDispOrderNo(onePositionSetOrderList[j].orderNo, onePositionSetOrderList[j].orderSubNo)
        _resultRecord.isDraw = j === 0 ? true : false

        _positionSetOrderMixList.push(_resultRecord)
      }
    //決済注文無
    }else { 
      _resultRecord = Object.assign(positionList[i])
      _resultRecord.setOrder = {}
      _resultRecord._key = _resultRecord.positionNo + ':noneOrder'
      _resultRecord.isDraw = true

      _positionSetOrderMixList.push(_resultRecord)
    }
  }

  return _positionSetOrderMixList
}


const _createSummaryList = (positionList, brandList, brandKeyMapList) => {
  let _positionSummaryMap = {}
  let brandCd = ''
  let sellBuyDiv = ''
  let summaryRecord = {}
  let date1 = null;
  let date2 = null;
  let date = null;

  //１次集計
  for(let i=0; i<positionList.length; i++){
    brandCd = positionList[i].brandCd
    sellBuyDiv = positionList[i].sellBuyDiv
    if(!(_positionSummaryMap[brandCd+':'+sellBuyDiv])) {
      summaryRecord = {
        key: positionList[i].brandCd + ':' + positionList[i].sellBuyDiv,
        cfdAccType: positionList[i].cfdAccType,         //CFD口座種別
        brandCd: positionList[i].brandCd,               //銘柄コード
        sellBuyDiv: positionList[i].sellBuyDiv,         //売買区分
        amount: positionList[i].amount,                 //取引数量
        lastContractDate: positionList[i].contractDate, //最終成立日時
        wkPriceSum: BigNumber(positionList[i].contractRate).times(BigNumber(positionList[i].amount)),  //平均成立値 算出一時値
        currRate: positionList[i].currRate,             //現在値
        pips: positionList[i].pips,                     //pips
        profit: positionList[i].profit,                 //評価損益
      }
    } else {
      summaryRecord = _positionSummaryMap[brandCd+':'+sellBuyDiv]
      //取引数量 - 加算
      summaryRecord.amount = parseInt(BigNumber(summaryRecord.amount).plus(BigNumber(positionList[i].amount)).toString())
      //最終成立日時
      date1 = dateFormat.parse(summaryRecord.lastContractDate);
      date2 = dateFormat.parse(positionList[i].contractDate);
      date = calcSubTime(date1, date2, 0) >= 0 ? positionList[i].contractDate : summaryRecord.lastContractDate
      summaryRecord.lastContractDate = date
      //平均成立値段 算出一時値
      summaryRecord.wkPriceSum = parseFloat((BigNumber(summaryRecord.wkPriceSum)).plus(BigNumber(positionList[i].contractRate).times(BigNumber(positionList[i].amount))).toString())
      //pips値加算
      summaryRecord.pips = parseFloat((BigNumber(summaryRecord.pips)).plus(BigNumber(positionList[i].pips))).toString()
      //評価損益加算
      summaryRecord.profit = parseFloat((BigNumber(summaryRecord.profit)).plus(BigNumber(positionList[i].profit))).toString()
    }
    _positionSummaryMap[brandCd+':'+sellBuyDiv] = summaryRecord
  }

  //平均成立値算出
  let val = null
  let aveContractRate = null
  let decDigit = null
  for (let key in _positionSummaryMap) {
    val = _positionSummaryMap[key]
    aveContractRate = (BigNumber(val.wkPriceSum).div(BigNumber(val.amount)))
    decDigit = getRateDigit(val.brandCd, brandKeyMapList)
    aveContractRate = aveContractRate.dp(decDigit);
    _positionSummaryMap[key].aveContractRate = aveContractRate.toString()
  }

  //最終リスト整形
  let _positionSummaryList = []
  let key = ''
  for(let i=0; i<brandList.length; i++) {
    key = brandList[i].brandCd + ':' + SELL_BUY.SELL
    if(_positionSummaryMap[key]) {
      _positionSummaryList.push(_positionSummaryMap[key])
    }
    key = brandList[i].brandCd + ':' + SELL_BUY.BUY
    if(_positionSummaryMap[key]) {
      _positionSummaryList.push(_positionSummaryMap[key])
    }
  }

  return _positionSummaryList
}

const getSelectableCdfAccList = (positionList) => {
  let existMap = []
  for(let i=0; i<positionList.length; i++) {
    existMap[positionList[i].cfdAccType] = true
  }

  let selectableList = []
  for(let i=0; i<SELECT_LIST_CFD_ACC_TYPE.length; i++) {
    if(SELECT_LIST_CFD_ACC_TYPE[i].key === SELECT_ALL || existMap[SELECT_LIST_CFD_ACC_TYPE[i].key] !== undefined) {
      selectableList.push(SELECT_LIST_CFD_ACC_TYPE[i])
    }
  }

  return selectableList
}

const getSelectableBrandList = (brandList, positionList) => {
  let existMap = []
  for(let i=0; i<positionList.length; i++) {
    existMap[positionList[i].brandCd] = true
  }

  const allBrandList = getSelectBrandList(brandList)
  let selectableList = []
  for(let i=0; i<allBrandList.length; i++) {
    if(allBrandList[i].key === SELECT_ALL || existMap[allBrandList[i].key] !== undefined) {
      selectableList.push(allBrandList[i])
    }
  }

  return selectableList
}
