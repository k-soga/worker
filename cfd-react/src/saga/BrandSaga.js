import logger from '../env/Logger'
import { put } from 'redux-saga/effects';
import { onGetBrandData } from '../store/data/BrandSlice'

export function* sg_getBrandData(action) {
  logger.log('called sg_getBrandData')

  //TODO モックアップ用静的現在値リスト作成
  const brandList = _getDummyInitBrandData()
  let brandKeyMapList = {}
  for(let i=0; i<brandList.length; i++) {
    brandKeyMapList[brandList[i].brandCd] = brandList[i]
 }

  yield put(onGetBrandData({brandList: brandList, brandKeyMapList: brandKeyMapList}))
  
  return { brandList: brandList, brandKeyMapList: brandKeyMapList }
}

const _getDummyInitBrandData = () => {
  //株式ST(Stock)、商品CM(Commodities)
  let _brandList = [] 
  //日本225
  _brandList.push({
      brandCd: 'ST001',                //銘柄コード
      brandName: '日本225',            //銘柄名称
      brandShortName1: '日本225',      //銘柄名略称1
      brandShortName2: '日本225',      //銘柄名略称2
      brandEnglishName: 'JP225',       //銘柄名称英記号
      cfdAccType: 1,                   //CFD口座種別コード
      rateIntegerDigit: 5,             //レート整数部桁数
      rateDecimalDigit: 0,             //レート少数部桁数
      basicTradeAmount: 10000,         //基本取引数量単位
      minTradeAmount: 1000,            //最小取引数量単位
      pips: 1,                         //基本値幅刻み
      maxSpecifiablePrice: 40000,      //最低指定可能値段
      minSpecifiablePrice: 20000,      //最高指定可能値段
  })
  //米国DOW30
  _brandList.push({
    brandCd: 'ST002',                //銘柄コード
    brandName: '米国DOW30',          //銘柄名称
    brandShortName1: '米国DOW30',    //銘柄名略称1
    brandShortName2: '米国DOW30',    //銘柄名略称2
    brandEnglishName: 'USD30',       //銘柄名称英記号
    cfdAccType: 1,                   //CFD口座種別コード
    rateIntegerDigit: 5,             //レート整数部桁数
    rateDecimalDigit: 0,             //レート少数部桁数
    basicTradeAmount: 10000,         //基本取引数量単位
    minTradeAmount: 1000,            //最小取引数量単位
    pips: 1,                         //基本値幅刻み
    maxSpecifiablePrice: 40000,      //最低指定可能値段
    minSpecifiablePrice: 20000,      //最高指定可能値段
  })
  //米国S&P500
  _brandList.push({
    brandCd: 'ST003',                //銘柄コード
    brandName: '米国S&P500',         //銘柄名称
    brandShortName1: '米国S&P500',   //銘柄名略称1
    brandShortName2: '米国S&P500',   //銘柄名略称2
    brandEnglishName: 'USSP500',     //銘柄名称英記号
    cfdAccType: 1,                   //CFD口座種別コード
    rateIntegerDigit: 4,             //レート整数部桁数
    rateDecimalDigit: 1,             //レート少数部桁数
    basicTradeAmount: 10000,         //基本取引数量単位
    minTradeAmount: 1000,            //最小取引数量単位
    pips: 0.1,                       //基本値幅刻み
    maxSpecifiablePrice: 5000.0,     //最低指定可能値段
    minSpecifiablePrice: 2000.0,     //最高指定可能値段
  })
  //米国NQ100
  _brandList.push({
    brandCd: 'ST004',                //銘柄コード
    brandName: '米国NQ100',          //銘柄名称
    brandShortName1: '米国NQ100',    //銘柄名略称1
    brandShortName2: '米国NQ100',    //銘柄名略称2
    brandEnglishName: 'USNQ100',     //銘柄名称英記号
    cfdAccType: 1,                   //CFD口座種別コード
    rateIntegerDigit: 5,             //レート整数部桁数
    rateDecimalDigit: 1,             //レート少数部桁数
    basicTradeAmount: 10000,         //基本取引数量単位
    minTradeAmount: 1000,            //最小取引数量単位
    pips: 0.1,                       //基本値幅刻み
    maxSpecifiablePrice: 20000.0,    //最低指定可能値段
    minSpecifiablePrice: 8000.0,     //最高指定可能値段
  })
  //金
  _brandList.push({
    brandCd: 'CM001',                //銘柄コード
    brandName: '金',                 //銘柄名称
    brandShortName1: '金',           //銘柄名略称1
    brandShortName2: '金',           //銘柄名略称2
    brandEnglishName: 'GOLD',        //銘柄名称英記号
    cfdAccType: 2,                   //CFD口座種別コード
    rateIntegerDigit: 4,             //レート整数部桁数
    rateDecimalDigit: 1,             //レート少数部桁数
    basicTradeAmount: 10000,         //基本取引数量単位
    minTradeAmount: 1000,            //最小取引数量単位
    pips: 0.1,                       //基本値幅刻み
    maxSpecifiablePrice: 3000.0,     //最低指定可能値段
    minSpecifiablePrice: 1000.0,     //最高指定可能値段
  })
  //銀
  _brandList.push({
    brandCd: 'CM002',                //銘柄コード
    brandName: '銀',                 //銘柄名称
    brandShortName1: '銀',           //銘柄名略称1
    brandShortName2: '銀',           //銘柄名略称2
    brandEnglishName: 'SILVER',      //銘柄名称英記号
    cfdAccType: 2,                   //CFD口座種別コード
    rateIntegerDigit: 2,             //レート整数部桁数
    rateDecimalDigit: 3,             //レート少数部桁数
    basicTradeAmount: 10000,         //基本取引数量単位
    minTradeAmount: 1000,            //最小取引数量単位
    pips: 0.001,                     //基本値幅刻み
    maxSpecifiablePrice: 99.000,     //最低指定可能値段
    minSpecifiablePrice: 2.000,      //最高指定可能値段
  })
  //原油
  _brandList.push({
    brandCd: 'CM003',              //銘柄コード
    brandName: '原油',             //銘柄名称
    brandShortName1: '原油',       //銘柄名略称1
    brandShortName2: '原油',       //銘柄名略称2
    brandEnglishName: 'OIL',       //銘柄名称英記号
    cfdAccType: 2,                   //CFD口座種別コード
    rateIntegerDigit: 2,           //レート整数部桁数
    rateDecimalDigit: 2,           //レート少数部桁数
    basicTradeAmount: 10000,       //基本取引数量単位
    minTradeAmount: 1000,          //最小取引数量単位
    pips: 0.01,                    //基本値幅刻み
    maxSpecifiablePrice: 99.00,    //最低指定可能値段
    minSpecifiablePrice: 10.00,    //最高指定可能値段
  })
  //天然ガス
  _brandList.push({
    brandCd: 'CM004',              //銘柄コード
    brandName: '天然ガス',          //銘柄名称
    brandShortName1: '天然ガス',    //銘柄名略称1
    brandShortName2: '天然ガス',    //銘柄名略称2
    brandEnglishName: 'GUS',       //銘柄名称英記号
    cfdAccType: 2,                   //CFD口座種別コード
    rateIntegerDigit: 1,           //レート整数部桁数
    rateDecimalDigit: 3,           //レート少数部桁数
    basicTradeAmount: 10000,       //基本取引数量単位
    minTradeAmount: 1000,          //最小取引数量単位
    pips: 0.001,                   //基本値幅刻み
    maxSpecifiablePrice: 9.000,    //最低指定可能値段
    minSpecifiablePrice: 0.500,    //最高指定可能値段
  })
  //イギリス100
  _brandList.push({
    brandCd: 'ST005',               //銘柄コード
    brandName: 'イギリス100',        //銘柄名称
    brandShortName1: 'イギリス100',  //銘柄名略称1
    brandShortName2: 'イギリス100',  //銘柄名略称2
    brandEnglishName: 'ENG100',     //銘柄名称英記号
    cfdAccType: 1,                   //CFD口座種別コード
    rateIntegerDigit: 4,            //レート整数部桁数
    rateDecimalDigit: 1,            //レート少数部桁数
    basicTradeAmount: 10000,        //基本取引数量単位
    minTradeAmount: 1000,           //最小取引数量単位
    pips: 0.1,                      //基本値幅刻み
    maxSpecifiablePrice: 9900.0,    //最低指定可能値段
    minSpecifiablePrice: 3000.0,    //最高指定可能値段
  })
  //ドイツ30
  _brandList.push({
    brandCd: 'ST006',               //銘柄コード
    brandName: 'ドイツ30',          //銘柄名称
    brandShortName1: 'ドイツ30',    //銘柄名略称1
    brandShortName2: 'ドイツ30',    //銘柄名略称2
    brandEnglishName: 'GMY30',      //銘柄名称英記号
    cfdAccType: 1,                   //CFD口座種別コード
    rateIntegerDigit: 5,            //レート整数部桁数
    rateDecimalDigit: 1,            //レート少数部桁数
    basicTradeAmount: 10000,        //基本取引数量単位
    minTradeAmount: 1000,           //最小取引数量単位
    pips: 0.1,                      //基本値幅刻み
    maxSpecifiablePrice: 20000.0,   //最低指定可能値段
    minSpecifiablePrice: 9000.0,    //最高指定可能値段
  })

  //ドイツ30
  _brandList.push({
    brandCd: 'ST007',               //銘柄コード
    brandName: 'ドイツ30',          //銘柄名称
    brandShortName1: 'ドイツ30',    //銘柄名略称1
    brandShortName2: 'ドイツ30',    //銘柄名略称2
    brandEnglishName: 'GMY30',      //銘柄名称英記号
    cfdAccType: 1,                   //CFD口座種別コード
    rateIntegerDigit: 5,            //レート整数部桁数
    rateDecimalDigit: 1,            //レート少数部桁数
    basicTradeAmount: 10000,        //基本取引数量単位
    minTradeAmount: 1000,           //最小取引数量単位
    pips: 0.1,                      //基本値幅刻み
    maxSpecifiablePrice: 20000.0,   //最低指定可能値段
    minSpecifiablePrice: 9000.0,    //最高指定可能値段
  })

  //ドイツ30
  _brandList.push({
    brandCd: 'ST008',               //銘柄コード
    brandName: 'ドイツ30',          //銘柄名称
    brandShortName1: 'ドイツ30',    //銘柄名略称1
    brandShortName2: 'ドイツ30',    //銘柄名略称2
    brandEnglishName: 'GMY30',      //銘柄名称英記号
    cfdAccType: 1,                  //CFD口座種別コード
    rateIntegerDigit: 5,            //レート整数部桁数
    rateDecimalDigit: 1,            //レート少数部桁数
    basicTradeAmount: 10000,        //基本取引数量単位
    minTradeAmount: 1000,           //最小取引数量単位
    pips: 0.1,                      //基本値幅刻み
    maxSpecifiablePrice: 20000.0,   //最低指定可能値段
    minSpecifiablePrice: 9000.0,    //最高指定可能値段
  })
  //ドイツ30
  _brandList.push({
    brandCd: 'ST009',               //銘柄コード
    brandName: 'ドイツ30',          //銘柄名称
    brandShortName1: 'ドイツ30',    //銘柄名略称1
    brandShortName2: 'ドイツ30',    //銘柄名略称2
    brandEnglishName: 'GMY30',      //銘柄名称英記号
    cfdAccType: 1,                   //CFD口座種別コード
    rateIntegerDigit: 5,            //レート整数部桁数
    rateDecimalDigit: 1,            //レート少数部桁数
    basicTradeAmount: 10000,        //基本取引数量単位
    minTradeAmount: 1000,           //最小取引数量単位
    pips: 0.1,                      //基本値幅刻み
    maxSpecifiablePrice: 20000.0,   //最低指定可能値段
    minSpecifiablePrice: 9000.0,    //最高指定可能値段
  })

  
  return _brandList
}
