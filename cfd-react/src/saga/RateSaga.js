import logger from '../env/Logger'
import { put } from 'redux-saga/effects';
import BigNumber from 'bignumber.js'
import { onGetInitRateData } from '../store/data/RateSlice'

export function* sg_getInitRateData(brandList) {
  logger.log('called sg_getInitRateData')

  //TODO モックアップ用静的現在値リスト作成
  let rateList = _getDummyInitRateData()
  rateList = _calculate(rateList)
  yield put(onGetInitRateData({rateList: rateList}))
  return rateList
}

const _calculate = (rateList) => {
  //スプレッド算出
  let bnSpread = 0;
  for(let i=0; i<rateList.length; i++) {
    bnSpread = BigNumber(rateList[i].offer).minus(BigNumber(rateList[i].bid))
    rateList[i].spread = parseFloat(bnSpread.toString())
  }
  return rateList
}

const _getDummyInitRateData = () => {
  //株式ST(Stock)、商品CM(Commodities)
  let _rateList = [] 
  //日本225
  _rateList.push({ brandCd: 'ST001', bid: 30211, offer: 30215, 
    high: 30559, low: 30127, start: 30350, end: 30340, comparePrevDay: -129, comparePrevPersent: -0.43,})
  //米国DOW30
  _rateList.push({ brandCd: 'ST002', bid: 31528, offer: 31533, 
    high: 31626, low: 31491, start: 31554, end: 31546, comparePrevDay: -18, comparePrevPersent: -0.06,})
  //米国S&P500
  _rateList.push({ brandCd: 'ST003', bid: 3919.7, offer: 3920.0, 
    high: 3935.7, low: 3914.0, start: 3927.6, end: 3925.7, comparePrevDay: -6.0, comparePrevPersent: -0.15,})
  //米国NQ100
  _rateList.push({ brandCd: 'ST004', bid: 13631.8, offer: 13633.3, 
    high: 3935.7, low: 3914.0, start: 3927.6, end: 3925.7, comparePrevDay: -6.0, comparePrevPersent: -0.15,})
  //金
  _rateList.push({ brandCd: 'CM001', bid: 1783.2, offer: 1783.6, 
    high: 1785.6, low: 1775.6, start: 1776.3, end: 1775.7, comparePrevDay: 7.5, comparePrevPersent: 0.42,})
  //銀
  _rateList.push({ brandCd: 'CM002', bid: 27.325, offer: 27.340, 
    high: 27.517, low: 27.229, start: 27.341, end: 27.345, comparePrevDay: -0.020, comparePrevPersent: -0.07,})
  //原油
  // _rateList.push({ brandCd: 'CM003', bid: 61.78, offer: 61.81, 
  _rateList.push({ brandCd: 'CM003', bid: 61.71, offer: 61.81, 
    high: 62.26, low: 61.40, start: 61.70, end: 61.72, comparePrevDay: 0.06, comparePrevPersent: 0.10,})
  //天然ガス
  _rateList.push({ brandCd: 'CM004', bid: 3.041, offer: 3.047, 
    high: 3.042, low: 3.023, start: 3.038, end: 3.233, comparePrevDay: -0.192, comparePrevPersent: -5.94})
  //イギリス100
  _rateList.push({ brandCd: 'ST005', bid: 6675.1, offer: 6676.4, 
    high: 6695.3, low: 6646.8, start: 6690.5, end: 6691.3, comparePrevDay: -16.2, comparePrevPersent: -0.24})
  //ドイツ30
  _rateList.push({ brandCd: 'ST006', bid: 13937.4, offer: 13941.2, 
    high: 14061.9, low: 13875.0, start: 14048.4, end: 14041.0, comparePrevDay: -103.6, comparePrevPersent: -0.74})

      //ドイツ30
  _rateList.push({ brandCd: 'ST007', bid: 13937.4, offer: 13941.2, 
    high: 14061.9, low: 13875.0, start: 14048.4, end: 14041.0, comparePrevDay: -103.6, comparePrevPersent: -0.74})
    //ドイツ30
  _rateList.push({ brandCd: 'ST008', bid: 13937.4, offer: 13941.2, 
    high: 14061.9, low: 13875.0, start: 14048.4, end: 14041.0, comparePrevDay: -103.6, comparePrevPersent: -0.74})
      //ドイツ30
  _rateList.push({ brandCd: 'ST009', bid: 13937.4, offer: 13941.2, 
    high: 14061.9, low: 13875.0, start: 14048.4, end: 14041.0, comparePrevDay: -103.6, comparePrevPersent: -0.74})

  return _rateList
}