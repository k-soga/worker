import { CFD_ACC_TYPE, SELL_BUY }  from '../../const/BizzConsts'

export const _getDummyInitPositionData = () => {
  let _positionList = [] 

  _positionList.push({
    positionNo: '0000001',             //委託者建玉No(ポジションNo.)
    brandCd: 'ST001',                  //銘柄コード
    cfdAccType: CFD_ACC_TYPE.STOCK,    //CFD口座種別
    sellBuyDiv: SELL_BUY.BUY,          //売買区分    
    contractDate: '2021/02/25 16:92:00',  //成立日時
    amount: 100000,                    //数量
    remainingAmount: 40000,            //決済可能残数量
    contractRate: 30315,               //成立値段
    profit: 12345678,                  //評価損益  ※口座種別でエンコード。要3桁区切り。※現在値での値洗い
  })

  _positionList.push({
    positionNo: '0000002',             //委託者建玉No(ポジションNo.)
    brandCd: 'CM001',                  //銘柄コード
    cfdAccType: CFD_ACC_TYPE.PRODUCT,    //CFD口座種別
    sellBuyDiv: SELL_BUY.BUY,          //売買区分    
    contractDate: '2021/02/25 16:92:00',  //成立日時
    amount: 40000,                     //数量
    remainingAmount: 40000,            //決済可能残数量
    contractRate: 1793.6,              //成立値段
    profit: 12345678,                  //評価損益  ※口座種別でエンコード。要3桁区切り。※現在値での値洗い
  })

  _positionList.push({
    positionNo: '0000003',             //委託者建玉No(ポジションNo.)
    brandCd: 'CM001',                  //銘柄コード
    cfdAccType: CFD_ACC_TYPE.PRODUCT,    //CFD口座種別
    sellBuyDiv: SELL_BUY.SELL,          //売買区分    
    contractDate: '2021/02/25 16:92:00',  //成立日時
    amount: 10000,                    //数量
    remainingAmount: 10000,            //決済可能残数量
    contractRate: 1783.2,              //成立値段
    profit: 12345678,                  //評価損益  ※口座種別でエンコード。要3桁区切り。※現在値での値洗い
  })

  _positionList.push({
    positionNo: '0000004',             //委託者建玉No(ポジションNo.)
    brandCd: 'CM001',                  //銘柄コード
    cfdAccType: CFD_ACC_TYPE.PRODUCT,    //CFD口座種別
    sellBuyDiv: SELL_BUY.SELL,          //売買区分    
    contractDate: '2021/02/25 16:92:00',  //成立日時
    amount: 200000,                    //数量
    remainingAmount: 200000,           //決済可能残数量
    contractRate: 1823.2,               //成立値段
    profit: 12345678,                  //評価損益  ※口座種別でエンコード。要3桁区切り。※現在値での値洗い
  })

  _positionList.push({
    positionNo: '0000005',             //委託者建玉No(ポジションNo.)
    brandCd: 'ST001',                  //銘柄コード
    cfdAccType: CFD_ACC_TYPE.STOCK,    //CFD口座種別
    sellBuyDiv: SELL_BUY.BUY,          //売買区分    
    contractDate: '2021/02/25 16:92:00',  //成立日時
    amount: 100000,                    //数量
    remainingAmount: 100000,           //決済可能残数量
    contractRate: 30095,               //成立値段
    profit: 12345678,                  //評価損益  ※口座種別でエンコード。要3桁区切り。※現在値での値洗い
  })

  _positionList.push({
    positionNo: '0000006',             //委託者建玉No(ポジションNo.)
    brandCd: 'CM001',                  //銘柄コード
    cfdAccType: CFD_ACC_TYPE.PRODUCT,    //CFD口座種別
    sellBuyDiv: SELL_BUY.BUY,          //売買区分    
    contractDate: '2021/02/25 16:92:00',  //成立日時
    amount: 40000,                     //数量
    remainingAmount: 0,                //決済可能残数量
    contractRate: 1816.6,              //成立値段
    profit: 12345678,                  //評価損益  ※口座種別でエンコード。要3桁区切り。※現在値での値洗い
  })

  _positionList.push({
    positionNo: '0000007',             //委託者建玉No(ポジションNo.)
    brandCd: 'CM001',                  //銘柄コード
    cfdAccType: CFD_ACC_TYPE.PRODUCT,    //CFD口座種別
    sellBuyDiv: SELL_BUY.SELL,          //売買区分    
    contractDate: '2021/02/25 16:92:00',  //成立日時
    amount: 10000,                    //数量
    remainingAmount: 10000,            //決済可能残数量
    contractRate: 1744.2,              //成立値段
    profit: 12345678,                  //評価損益  ※口座種別でエンコード。要3桁区切り。※現在値での値洗い
  })

  _positionList.push({
    positionNo: '0000008',             //委託者建玉No(ポジションNo.)
    brandCd: 'CM001',                  //銘柄コード
    cfdAccType: CFD_ACC_TYPE.PRODUCT,    //CFD口座種別
    sellBuyDiv: SELL_BUY.SELL,          //売買区分    
    contractDate: '2021/02/25 16:92:00',  //成立日時
    amount: 200000,                    //数量
    remainingAmount: 200000,           //決済可能残数量
    contractRate: 1698.1,               //成立値段
    profit: 12345678,                  //評価損益  ※口座種別でエンコード。要3桁区切り。※現在値での値洗い
  })

  _positionList.push({
    positionNo: '0000009',             //委託者建玉No(ポジションNo.)
    brandCd: 'ST001',                  //銘柄コード
    cfdAccType: CFD_ACC_TYPE.STOCK,    //CFD口座種別
    sellBuyDiv: SELL_BUY.BUY,          //売買区分    
    contractDate: '2021/02/25 16:92:00',  //成立日時
    amount: 100000,                    //数量
    remainingAmount: 100000,           //決済可能残数量
    contractRate: 30211,               //成立値段
    profit: 12345678,                  //評価損益  ※口座種別でエンコード。要3桁区切り。※現在値での値洗い
  })

  _positionList.push({
    positionNo: '0000010',             //委託者建玉No(ポジションNo.)
    brandCd: 'CM001',                  //銘柄コード
    cfdAccType: CFD_ACC_TYPE.PRODUCT,    //CFD口座種別
    sellBuyDiv: SELL_BUY.BUY,          //売買区分    
    contractDate: '2021/02/25 16:92:00',  //成立日時
    amount: 40000,                     //数量
    remainingAmount: 40000,            //決済可能残数量
    contractRate: 1783.9,              //成立値段
    profit: 12345678,                  //評価損益  ※口座種別でエンコード。要3桁区切り。※現在値での値洗い
  })

  _positionList.push({
    positionNo: '0000011',             //委託者建玉No(ポジションNo.)
    brandCd: 'CM001',                  //銘柄コード
    cfdAccType: CFD_ACC_TYPE.PRODUCT,    //CFD口座種別
    sellBuyDiv: SELL_BUY.SELL,          //売買区分    
    contractDate: '2021/02/25 16:92:00',  //成立日時
    amount: 10000,                    //数量
    remainingAmount: 10000,            //決済可能残数量
    contractRate: 1789.2,              //成立値段
    profit: 12345678,                  //評価損益  ※口座種別でエンコード。要3桁区切り。※現在値での値洗い
  })

  _positionList.push({
    positionNo: '0000012',             //委託者建玉No(ポジションNo.)
    brandCd: 'CM001',                  //銘柄コード
    cfdAccType: CFD_ACC_TYPE.PRODUCT,    //CFD口座種別
    sellBuyDiv: SELL_BUY.SELL,          //売買区分    
    contractDate: '2021/02/25 16:92:00',  //成立日時
    amount: 200000,                    //数量
    remainingAmount: 200000,           //決済可能残数量
    contractRate: 1803.2,               //成立値段
    profit: 12345678,                  //評価損益  ※口座種別でエンコード。要3桁区切り。※現在値での値洗い
  })

  return _positionList
}