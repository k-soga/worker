import { CFD_ACC_TYPE, SELL_BUY, EFFECT_PERIOD, EXEC_CONDITION, ORDER_TYPE }  from '../../const/BizzConsts'

export const _getDummyInitSetOrderData = () => {
   let _setOrderList = [] 

  _setOrderList.push({
    orderNo: '3000000001',                //注文番号
    orderSubNo: '01',                     //注文番号枝番
    brandCd: 'ST001',                     //銘柄コード
    cfdAccType: CFD_ACC_TYPE.STOCK,       //CFD口座種別
    sellBuyDiv: SELL_BUY.SELL,            //売買区分
    amount: 20000,                        //数量
    execCondition: EXEC_CONDITION.POINT,  //執行条件
    orderType: ORDER_TYPE.IFO,            //注文タイプ
    pointPrice: 30315,                    //指定値段
    ocoPair: {
        orderNo: '3000000002',            //OCO相手注文番号
        orderSubNo: '01',                 //OCO相手注文番号枝番
        pointPrice: 30315,                //OCO相手指定値段
    },
    effectivePeriod: EFFECT_PERIOD.WEEK,  //有効期限
    orderDate: '2021/03/01 10:00:00',     //受注日時
    targetPosition: {
      positionNo: '0000001',              //決済対象ポジション番号
      contractRate: 30315,                //成立値段
    }
  })

  _setOrderList.push({
    orderNo: '3000000003',                //注文番号
    orderSubNo: '01',                     //注文番号枝番
    brandCd: 'ST001',                     //銘柄コード
    cfdAccType: CFD_ACC_TYPE.STOCK,       //CFD口座種別
    sellBuyDiv: SELL_BUY.SELL,            //売買区分
    amount: 40000,                        //数量
    execCondition: EXEC_CONDITION.POINT,  //執行条件
    orderType: ORDER_TYPE.SINGLE_POINT,   //注文タイプ
    pointPrice: 30315,                    //指定値段
    ocoPair: {
        orderNo: null,                    //OCO相手注文番号
        orderSubNo: null,                 //OCO相手注文番号枝番
        pointPrice: null,                 //OCO相手指定値段
    },
    effectivePeriod: EFFECT_PERIOD.WEEK,  //有効期限
    orderDate: '2021/03/01 10:00:00',     //受注日時
    targetPosition: {
      positionNo: '0000001',              //決済対象ポジション番号
      contractRate: 30315,                //成立値段
    }
  })

  _setOrderList.push({
    orderNo: '3000000004',                //注文番号
    orderSubNo: '01',                     //注文番号枝番
    brandCd: 'CM001',                     //銘柄コード
    cfdAccType: CFD_ACC_TYPE.PRODUCT,     //CFD口座種別
    sellBuyDiv: SELL_BUY.SELL,            //売買区分
    amount: 40000,                        //数量
    execCondition: EXEC_CONDITION.POINT,  //執行条件
    orderType: ORDER_TYPE.SINGLE_POINT,   //注文タイプ
    pointPrice: 1816.6,                   //指定値段
    ocoPair: {
        orderNo: null,                    //OCO相手注文番号
        orderSubNo: null,                 //OCO相手注文番号枝番
        pointPrice: null,                 //OCO相手指定値段
    },
    effectivePeriod: EFFECT_PERIOD.WEEK,  //有効期限
    orderDate: '2021/03/01 10:00:00',     //受注日時
    targetPosition: {
      positionNo: '0000006',              //決済対象ポジション番号
      contractRate: 1816.6,               //成立値段
    }
  })

  return _setOrderList
}
