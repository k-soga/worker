import { CFD_ACC_TYPE, SELL_BUY, EFFECT_PERIOD, EXEC_CONDITION, ORDER_TYPE }  from '../../const/BizzConsts'

export const _getDummyInitNewOrderData = () => {
  let _newOrderList = [] 

  _newOrderList.push({
    orderNo: '1000000001',                //注文番号
    orderSubNo: '01',                     //注文番号枝番
    brandCd: 'ST001',                     //銘柄コード
    cfdAccType: CFD_ACC_TYPE.STOCK,       //CFD口座種別
    sellBuyDiv: SELL_BUY.BUY,             //売買区分
    amount: 100000,                       //数量
    execCondition: EXEC_CONDITION.POINT,  //執行条件
    orderType: ORDER_TYPE.IFO,            //注文タイプ
    pointPrice: 30315,                    //指定値段
    effectivePeriod: EFFECT_PERIOD.WEEK,  //有効期限
    deposit: 0,                           //必要保証金
    orderDate: '2021/03/01 10:00:00',     //受注日時
    ifdIfoSetOrder: {
      orderNo_point: '2000000100',        //IFDIFO決済注文(指値) 注文番号
      orderSubNo_point: '01',             //IFDIFO決済注文(指値) 注文番号枝番
      pointPrice_point: 30315,            //IFDIFO決済注文(指値) 指定値段
      orderNo_revPoint: '2000000200',     //IFDIFO決済注文(逆指値) 注文番号
      orderSubNo_revPoint: '01',          //IFDIFO決済注文(逆指値) 注文番号枝番
      pointPrice_revPoint:  30315,        //IFDIFO決済注文(逆指値) 指定値段
    }
  })
 
  _newOrderList.push({
    orderNo: '1000000002',                //注文番号
    orderSubNo: '01',                     //注文番号枝番
    brandCd: 'CM001',                     //銘柄コード
    cfdAccType: CFD_ACC_TYPE.PRODUCT,     //CFD口座種別
    sellBuyDiv: SELL_BUY.SELL,            //売買区分    
    amount: 100000,                       //数量
    execCondition: EXEC_CONDITION.POINT,  //執行条件
    orderType: ORDER_TYPE.IFO,            //注文タイプ
    pointPrice: 1783.2	,                 //指定値段
    effectivePeriod: EFFECT_PERIOD.WEEK,  //有効期限
    deposit: 0,                           //必要保証金
    orderDate: '2021/03/01 10:00:00',     //受注日時
    ifdIfoSetOrder: {
      orderNo_point: '2000000002',        //IFDIFO決済注文(指値) 注文番号
      orderSubNo_point: '01',             //IFDIFO決済注文(指値) 注文番号枝番
      pointPrice_point: 1783.2,           //IFDIFO決済注文(指値) 指定値段
      orderNo_revPoint: '2000000102',     //IFDIFO決済注文(逆指値) 注文番号
      orderSubNo_revPoint: '01',          //IFDIFO決済注文(逆指値) 注文番号枝番
      pointPrice_revPoint: 1783.2,        //IFDIFO決済注文(逆指値) 指定値段
    }
  })

  _newOrderList.push({
    orderNo: '1000000003',                //注文番号
    orderSubNo: '01',                     //注文番号枝番
    brandCd: 'ST001',                     //銘柄コード
    cfdAccType: CFD_ACC_TYPE.STOCK,       //CFD口座種別
    sellBuyDiv: SELL_BUY.SELL,            //売買区分    
    amount: 100000,                       //数量
    execCondition: EXEC_CONDITION.POINT,  //執行条件
    orderType: ORDER_TYPE.IFO,            //注文タイプ
    pointPrice: 30315,                    //指定値段
    effectivePeriod: EFFECT_PERIOD.WEEK,  //有効期限
    deposit: 0,                           //必要保証金
    orderDate: '2021/03/01 10:00:00',     //受注日時
    ifdIfoSetOrder: {
      orderNo_point: '2000000003',        //IFDIFO決済注文(指値) 注文番号
      orderSubNo_point: '01',             //IFDIFO決済注文(指値) 注文番号枝番
      pointPrice_point: 30315,            //IFDIFO決済注文(指値) 指定値段
      orderNo_revPoint: '2000000103',     //IFDIFO決済注文(逆指値) 注文番号
      orderSubNo_revPoint: '01',          //IFDIFO決済注文(逆指値) 注文番号枝番
      pointPrice_revPoint:  30315,        //IFDIFO決済注文(逆指値) 指定値段
    }
  })
 
  _newOrderList.push({
    orderNo: '1000000004',                //注文番号
    orderSubNo: '01',                     //注文番号枝番
    brandCd: 'CM001',                     //銘柄コード
    cfdAccType: CFD_ACC_TYPE.PRODUCT,     //CFD口座種別
    sellBuyDiv: SELL_BUY.SELL,            //売買区分    
    amount: 100000,                       //数量
    execCondition: EXEC_CONDITION.POINT,  //執行条件
    orderType: ORDER_TYPE.IFO,            //注文タイプ
    pointPrice: 1783.2	,                 //指定値段
    effectivePeriod: EFFECT_PERIOD.WEEK,  //有効期限
    deposit: 0,                           //必要保証金
    orderDate: '2021/03/01 10:00:00',     //受注日時
    ifdIfoSetOrder: {
      orderNo_point: '2000000004',        //IFDIFO決済注文(指値) 注文番号
      orderSubNo_point: '01',             //IFDIFO決済注文(指値) 注文番号枝番
      pointPrice_point: 1783.2,           //IFDIFO決済注文(指値) 指定値段
      orderNo_revPoint: '2000000104',     //IFDIFO決済注文(逆指値) 注文番号
      orderSubNo_revPoint: '01',          //IFDIFO決済注文(逆指値) 注文番号枝番
      pointPrice_revPoint: 1783.2,        //IFDIFO決済注文(逆指値) 指定値段
    }
  })

  _newOrderList.push({
    orderNo: '1000000005',                //注文番号
    orderSubNo: '01',                     //注文番号枝番
    brandCd: 'ST001',                     //銘柄コード
    cfdAccType: CFD_ACC_TYPE.STOCK,       //CFD口座種別
    sellBuyDiv: SELL_BUY.SELL,            //売買区分    
    amount: 100000,                       //数量
    execCondition: EXEC_CONDITION.POINT,  //執行条件
    orderType: ORDER_TYPE.IFO,            //注文タイプ
    pointPrice: 30315,                    //指定値段
    effectivePeriod: EFFECT_PERIOD.WEEK,  //有効期限
    deposit: 0,                           //必要保証金
    orderDate: '2021/03/01 10:00:00',     //受注日時
    ifdIfoSetOrder: {
      orderNo_point: '2000000005',        //IFDIFO決済注文(指値) 注文番号
      orderSubNo_point: '01',             //IFDIFO決済注文(指値) 注文番号枝番
      pointPrice_point: 30315,            //IFDIFO決済注文(指値) 指定値段
      orderNo_revPoint: '2000000105',     //IFDIFO決済注文(逆指値) 注文番号
      orderSubNo_revPoint: '01',          //IFDIFO決済注文(逆指値) 注文番号枝番
      pointPrice_revPoint:  30315,        //IFDIFO決済注文(逆指値) 指定値段
    }
  })
 
  _newOrderList.push({
    orderNo: '1000000007',                //注文番号
    orderSubNo: '01',                     //注文番号枝番
    brandCd: 'CM001',                     //銘柄コード
    cfdAccType: CFD_ACC_TYPE.PRODUCT,     //CFD口座種別
    sellBuyDiv: SELL_BUY.SELL,            //売買区分    
    amount: 100000,                       //数量
    execCondition: EXEC_CONDITION.POINT,  //執行条件
    orderType: ORDER_TYPE.IFO,            //注文タイプ
    pointPrice: 1783.2	,                 //指定値段
    effectivePeriod: EFFECT_PERIOD.WEEK,  //有効期限
    deposit: 0,                           //必要保証金
    orderDate: '2021/03/01 10:00:00',     //受注日時
    ifdIfoSetOrder: {
      orderNo_point: '2000000007',        //IFDIFO決済注文(指値) 注文番号
      orderSubNo_point: '01',             //IFDIFO決済注文(指値) 注文番号枝番
      pointPrice_point: 1783.2,           //IFDIFO決済注文(指値) 指定値段
      orderNo_revPoint: '2000000107',     //IFDIFO決済注文(逆指値) 注文番号
      orderSubNo_revPoint: '01',          //IFDIFO決済注文(逆指値) 注文番号枝番
      pointPrice_revPoint: 1783.2,        //IFDIFO決済注文(逆指値) 指定値段
    }
  })

  _newOrderList.push({
    orderNo: '1000000008',                //注文番号
    orderSubNo: '01',                     //注文番号枝番
    brandCd: 'CM001',                     //銘柄コード
    cfdAccType: CFD_ACC_TYPE.PRODUCT,     //CFD口座種別
    sellBuyDiv: SELL_BUY.SELL,            //売買区分    
    amount: 100000,                       //数量
    execCondition: EXEC_CONDITION.POINT,  //執行条件
    orderType: ORDER_TYPE.IFO,            //注文タイプ
    pointPrice: 1783.2,                   //指定値段
    effectivePeriod: EFFECT_PERIOD.WEEK,  //有効期限
    deposit: 0,                           //必要保証金
    orderDate: '2021/03/01 10:00:00',     //受注日時
    ifdIfoSetOrder: {
      orderNo_point: '2000000008',        //IFDIFO決済注文(指値) 注文番号
      orderSubNo_point: '01',             //IFDIFO決済注文(指値) 注文番号枝番
      pointPrice_point: 1783.2,           //IFDIFO決済注文(指値) 指定値段
      orderNo_revPoint: '2000000108',     //IFDIFO決済注文(逆指値) 注文番号
      orderSubNo_revPoint: '01',          //IFDIFO決済注文(逆指値) 注文番号枝番
      pointPrice_revPoint: 1783.2,        //IFDIFO決済注文(逆指値) 指定値段
    }
  })

  _newOrderList.push({
    orderNo: '1000000009',                //注文番号
    orderSubNo: '01',                     //注文番号枝番
    brandCd: 'CM001',                     //銘柄コード
    cfdAccType: CFD_ACC_TYPE.PRODUCT,     //CFD口座種別
    sellBuyDiv: SELL_BUY.SELL,            //売買区分    
    amount: 100000,                       //数量
    execCondition: EXEC_CONDITION.POINT,  //執行条件
    orderType: ORDER_TYPE.IFO,            //注文タイプ
    pointPrice: 1783.2,                   //指定値段
    effectivePeriod: EFFECT_PERIOD.WEEK,  //有効期限
    deposit: 0,                           //必要保証金
    orderDate: '2021/03/01 10:00:00',     //受注日時
    ifdIfoSetOrder: {
      orderNo_point: '2000000009',        //IFDIFO決済注文(指値) 注文番号
      orderSubNo_point: '01',             //IFDIFO決済注文(指値) 注文番号枝番
      pointPrice_point: 1783.2,           //IFDIFO決済注文(指値) 指定値段
      orderNo_revPoint: '2000000109',     //IFDIFO決済注文(逆指値) 注文番号
      orderSubNo_revPoint: '01',          //IFDIFO決済注文(逆指値) 注文番号枝番
      pointPrice_revPoint: 1783.2,        //IFDIFO決済注文(逆指値) 指定値段
    }
  })

  _newOrderList.push({
    orderNo: '1000000010',                //注文番号
    orderSubNo: '01',                     //注文番号枝番
    brandCd: 'CM001',                     //銘柄コード
    cfdAccType: CFD_ACC_TYPE.PRODUCT,     //CFD口座種別
    sellBuyDiv: SELL_BUY.SELL,            //売買区分    
    amount: 100000,                       //数量
    execCondition: EXEC_CONDITION.POINT,  //執行条件
    orderType: ORDER_TYPE.IFO,            //注文タイプ
    pointPrice: 1783.2,                   //指定値段
    effectivePeriod: EFFECT_PERIOD.WEEK,  //有効期限
    deposit: 0,                           //必要保証金
    orderDate: '2021/03/01 10:00:00',     //受注日時
    ifdIfoSetOrder: {
      orderNo_point: '2000000010',        //IFDIFO決済注文(指値) 注文番号
      orderSubNo_point: '01',             //IFDIFO決済注文(指値) 注文番号枝番
      pointPrice_point: 1783.2,           //IFDIFO決済注文(指値) 指定値段
      orderNo_revPoint: '2000000110',     //IFDIFO決済注文(逆指値) 注文番号
      orderSubNo_revPoint: '01',          //IFDIFO決済注文(逆指値) 注文番号枝番
      pointPrice_revPoint: 1783.2,        //IFDIFO決済注文(逆指値) 指定値段
    }
  })

  return _newOrderList
}
