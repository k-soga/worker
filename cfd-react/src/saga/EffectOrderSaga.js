import logger from '../env/Logger'
import { put } from 'redux-saga/effects';
import { onGetInitEffectOrderData } from '../store/data/EffectOrderSlice'
import { _getDummyInitNewOrderData } from './dummy/DummyNewOrder'
import { _getDummyInitSetOrderData } from './dummy/DummySetOrder'
import { SELECT_ALL, SELECT_LIST_CFD_ACC_TYPE } from '../const/BizzConsts'
import { getSelectBrandList } from '../utils/BizzUtils'

export function* sg_getInitEffectOrderData(brandList, brandKeyMapList) {
  logger.log('called sg_getInitPositionData')
  const newOrderList = _getDummyInitNewOrderData()
  const setOrderList = _getDummyInitSetOrderData()
  const setOrderMap2Position = _createSetOrderMap2Position(setOrderList)
  const newOrderSelectCdfAccList = getSelectableCdfAccList(newOrderList)
  const newOrderSelectBrandList = getSelectableBrandList(brandList, newOrderList)

  yield put(onGetInitEffectOrderData({
    newOrderList: newOrderList, 
    newOrderSelectCdfAccList: newOrderSelectCdfAccList, 
    newOrderSelectBrandList: newOrderSelectBrandList, 
    setOrderList: setOrderList, 
    setOrderMap2Position: setOrderMap2Position
  }))
  return setOrderMap2Position
}

const _createSetOrderMap2Position = (setOrderList) => {
  let _map = {}
  let curPositionNo = null;
  let pevPositionNo = null;
  let rowList = []

  for(let i=0; i<setOrderList.length; i++) {
    curPositionNo = setOrderList[i].targetPosition.positionNo
    if(curPositionNo !== pevPositionNo) {
      rowList = []
    } else {
      rowList = _map[curPositionNo]
    }
    rowList.push(setOrderList[i])
    _map[curPositionNo] = rowList

    pevPositionNo = curPositionNo
  }
  
  return _map
}

const getSelectableCdfAccList = (newOrderList) => {
  let existMap = []
  for(let i=0; i<newOrderList.length; i++) {
    existMap[newOrderList[i].cfdAccType] = true
  }

  let selectableList = []
  for(let i=0; i<SELECT_LIST_CFD_ACC_TYPE.length; i++) {
    if(SELECT_LIST_CFD_ACC_TYPE[i].key === SELECT_ALL || existMap[SELECT_LIST_CFD_ACC_TYPE[i].key] !== undefined) {
      selectableList.push(SELECT_LIST_CFD_ACC_TYPE[i])
    }
  }
  
  return selectableList
}

const getSelectableBrandList = (brandList, newOrderList) => {
  let existMap = []
  for(let i=0; i<newOrderList.length; i++) {
    existMap[newOrderList[i].brandCd] = true
  }

  const allBrandList = getSelectBrandList(brandList)
  let selectableList = []
  for(let i=0; i<allBrandList.length; i++) {
    if(allBrandList[i].key === SELECT_ALL || existMap[allBrandList[i].key] !== undefined) {
      selectableList.push(allBrandList[i])
    }
  }

  return selectableList
}