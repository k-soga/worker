import { useState, useEffect } from 'react';

export const useWindowDimensions = () => {

  const getWindowDimensions = () => {
    // const { innerWidth: width, innerHeight: height } = window;
    // const { dispHeight: dispHeight, availHeight: availHeight } = window.screen;
    const innerWidth = window.innerWidth;
    const innerHeight = window.innerHeight;
    const availHeight = window.screen.availHeight;
    const dispHeight = window.screen.dispHeight;

    return {
      innerWidth,
      innerHeight,
      availHeight,
      dispHeight
    };
  }

  const [windowDimensions, setWindowDimensions] = useState(getWindowDimensions());

  useEffect(() => {
    const onResize = () => {
      setWindowDimensions(getWindowDimensions());
    }
    window.addEventListener('resize', onResize);
    return () => window.removeEventListener('resize', onResize);
  }, []);

  return windowDimensions;
}
