import logger from '../env/Logger'
import { combineReducers } from 'redux'
import { configureStore, getDefaultMiddleware } from '@reduxjs/toolkit'
import { save, load } from 'redux-localstorage-simple';
import createSagaMiddleware from 'redux-saga'
import { actionInitReduxState } from '../actions/actions'
import { encSliceName } from '../utils/SystemUtils'
import rootSaga from '../saga/RootSagas'
import AppTopSlice from '../store/AppTopSlice'
import UiSettingSlice from '../store/ui/UiSettingSlice'
import HeadMenuSlice from '../store/ui/HeadMenuSlice'
import RateViewSlice from '../store/ui/RateViewSlice'
import PositionViewSlice from '../store/ui/PositionViewSlice'
import NewOrderModalSlice from '../store/ui/NewOrderModalSlice'
import BrandSlice from '../store/data/BrandSlice'
import RateSlice from '../store/data/RateSlice'
import PositionSlice from '../store/data/PositionSlice'
import EffectOrderSlice from '../store/data/EffectOrderSlice'

const getCombineReduceMap = () => {
  let combineReduceMap = {}
  combineReduceMap[encSliceName('uiSetting')] = UiSettingSlice;
  combineReduceMap[encSliceName('appTop')] = AppTopSlice;
  combineReduceMap[encSliceName('headMenu')] = HeadMenuSlice;
  combineReduceMap[encSliceName('rateView')] = RateViewSlice;
  combineReduceMap[encSliceName('positionView')] = PositionViewSlice;
  combineReduceMap[encSliceName('newOrderModal')] = NewOrderModalSlice;
  combineReduceMap[encSliceName('rate')] = RateSlice;
  combineReduceMap[encSliceName('brand')] = BrandSlice;
  combineReduceMap[encSliceName('position')] = PositionSlice;
  combineReduceMap[encSliceName('effectOrder')] = EffectOrderSlice;
  return combineReduceMap
}

const reducer = combineReducers(getCombineReduceMap());

const rootReducer = (state, action) => {
  const flag = (action.type === actionInitReduxState.type);
  return reducer(flag ? undefined : state, action);
};

function configureAppStore() {
  const sagaMiddleware = createSagaMiddleware(rootSaga);  
  const store = configureStore({
    reducer: rootReducer,
    preloadedState: load(),
    middleware: [sagaMiddleware,
              ...getDefaultMiddleware().concat(save())],
  })
  sagaMiddleware.run(rootSaga);
  store.subscribe(() =>
    logger.log(store.getState())
  )
  return store
}

export default configureAppStore()
