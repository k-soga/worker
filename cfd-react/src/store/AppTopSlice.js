import { createSlice } from '@reduxjs/toolkit';

export const AppTopSlice = createSlice({
  name: 'appTop',
  initialState: {
    isInitBizzData: false,
    windowInfo: {
      dispHeight: 1000,
    },
  },
  reducers: {
    initApp: (state, action) => {
      state.windowInfo = action.payload
    },
    onSuccessedInitBizzData: (state, action) => {
      state.isInitBizzData = true
    },
  }
});

export const { initApp, onSuccessedInitBizzData } = AppTopSlice.actions;

export default AppTopSlice.reducer;
