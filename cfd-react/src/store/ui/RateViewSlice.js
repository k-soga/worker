import { createSlice } from '@reduxjs/toolkit';
import { encSliceName } from '../../utils/SystemUtils'

export const RATE_MODE = { PANEL: 'panel', LIST: 'list', }
export const V_POSITION = { TOP: 'top', BOTTOM: 'bottom', }
export const MAXMIN_MODE = { MAX: 'max', MIN: 'min', }

export const RateViewSlice = createSlice({
  name: encSliceName('rateView'),
  initialState: {
    viewMode: RATE_MODE.PANEL,
    vertical: V_POSITION.TOP,
    maxmin: MAXMIN_MODE.MAX
  },
  reducers: {
    onChangeViewMode: (state, action) => {
      state.viewMode = action.payload
    },
    onChangeVerticalPosition: (state, action) => {
      state.vertical = action.payload
    },
    onChangeMaxMinViewMode: (state, action) => {
      state.maxmin = action.payload
    },
  }
});

export const { onChangeViewMode, onChangeVerticalPosition, onChangeMaxMinViewMode } = RateViewSlice.actions;

export default RateViewSlice.reducer;
