import { createSlice } from '@reduxjs/toolkit';
import { encSliceName } from '../../utils/SystemUtils'

export const MAIN_POSITION_MODE = { LIST: 'list', SUMMARY: 'summary' }

export const PositionViewSlice = createSlice({
  name: encSliceName('positionView'),
  initialState: {
    viewMode: MAIN_POSITION_MODE.LIST,
  },
  reducers: {
    onChangeViewMode: (state, action) => {
      state.viewMode = action.payload
    },
  }
});

export const { onChangeViewMode } = PositionViewSlice.actions;

export default PositionViewSlice.reducer;
