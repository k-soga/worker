import { createSlice } from '@reduxjs/toolkit';
import { encSliceName } from '../../utils/SystemUtils'

export const COLTYPES = {
  SILVER_BLUE: 'silver_blue',
  BLACK_GOLD: 'black_gold',
  PEARL_ROUGE: 'pearlrouge',
}

export const UiSettingSlice = createSlice({
  name: encSliceName('uiSetting'),
  initialState: {
    colorPattern: COLTYPES.SILVER_BLUE
  },
  reducers: {
    onChangeColorPattern: (state, action) => {
      state.colorPattern = action.payload
    },
  }
});

export const { onChangeColorPattern } = UiSettingSlice.actions;

export default UiSettingSlice.reducer;
