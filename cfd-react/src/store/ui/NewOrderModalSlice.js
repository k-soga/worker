import { createSlice } from '@reduxjs/toolkit';
import { encSliceName } from '../../utils/SystemUtils'

export const NewOrderModalSlice = createSlice({
  name: encSliceName('newOrderModal'),
  initialState: {
    isOpen: false
  },
  reducers: {
    onChangeOpen: (state, action) => {
      state.isOpen = action.payload
    },
  }
});

export const { onChangeOpen } = NewOrderModalSlice.actions;

export default NewOrderModalSlice.reducer;
