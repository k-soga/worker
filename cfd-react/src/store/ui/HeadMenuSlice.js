import { createSlice } from '@reduxjs/toolkit';
import { encSliceName } from '../../utils/SystemUtils'
import { MENU_CATEGORY_TYPES } from '../../const/Menus'

export const HeadMenuSlice = createSlice({
  name: encSliceName('headMenu'),
  initialState: {
    selectedMenuCategoryType: MENU_CATEGORY_TYPES.MAIN,
    isOpenSubMenu4Ref: false
  },
  reducers: {
    onChangeSelected: (state, action) => {
      state.selectedMenuCategoryType = action.payload
    },
    onClickRef: (state, action) => {
      state.isOpenSubMenu4Ref = !state.isOpenSubMenu4Ref
    },
    onCloseRef: (state, action) => {
      state.isOpenSubMenu4Ref = false
    },
    initMenuSelected: (state, action) => {
      state.selectedMenuCategoryType = MENU_CATEGORY_TYPES.MAIN
    },
  }
});

export const { onChangeSelected, onClickRef, onCloseRef, initMenuSelected } = HeadMenuSlice.actions;

export default HeadMenuSlice.reducer;
