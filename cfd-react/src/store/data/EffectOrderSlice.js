import { createSlice } from '@reduxjs/toolkit';
import { encSliceName } from '../../utils/SystemUtils'

export const EffectOrderSlice = createSlice({
  name: encSliceName('effectOrder'),
  initialState: {
    newOrderList: [],
    newOrderSelectCdfAccList: [],
    newOrderSelectBrandList: [],
    setOrderList: [],
    setOrderMap2Position: [],
  },
  reducers: {
    getInitEffectOrderData: (state, action) => {
    },
    onGetInitEffectOrderData: (state, action) => {
      state.newOrderList = action.payload.newOrderList
      state.newOrderSelectCdfAccList = action.payload.newOrderSelectCdfAccList
      state.newOrderSelectBrandList = action.payload.newOrderSelectBrandList
      state.setOrderList = action.payload.setOrderList
      state.setOrderMap2Position = action.payload.setOrderMap2Position
    },
  }
});

export const { getInitEffectOrderData, onGetInitEffectOrderData } = EffectOrderSlice.actions;

export default EffectOrderSlice.reducer;
