import { createSlice } from '@reduxjs/toolkit';
import { encSliceName } from '../../utils/SystemUtils'

export const RateSlice = createSlice({
  name: encSliceName('rate'),
  initialState: {
    currRateList: [],
    prevRateList: [],
  },
  reducers: {
    getInitRateData: (state, action) => {
    },
    onGetInitRateData: (state, action) => {
      state.currRateList = action.payload.rateList
      state.prevRateList = action.payload.rateList
    },
    getUpdateRateData: (state, action) => {
    },
    onGetUpdateRateData: (state, action) => {
      state.prevRateList = state.currRateList
      state.currRateList = action.payload.rateList
    },
  }
});

export const { getInitRateData, onGetInitRateData, getUpdateRateData, onGetUpdateRateData } = RateSlice.actions;

export default RateSlice.reducer;
