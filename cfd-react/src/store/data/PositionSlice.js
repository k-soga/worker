import { createSlice } from '@reduxjs/toolkit';
import { encSliceName } from '../../utils/SystemUtils'

export const PositionSlice = createSlice({
  name: encSliceName('position'),
  initialState: {
    positionList: [],
    positionInSetOrderList: [],
    positionSummaryList: [],
    positionSelectCdfAccList: [],
    positionSelectBrandList: [],
  },
  reducers: {
    getInitPositionData: (state, action) => {
    },
    onGetInitPositionData: (state, action) => {
      state.positionList = action.payload.positionList
      state.positionInSetOrderList = action.payload.positionInSetOrderList
      state.positionSummaryList = action.payload.positionSummaryList
      state.positionSelectCdfAccList = action.payload.positionSelectCdfAccList
      state.positionSelectBrandList = action.payload.positionSelectBrandList
    },
  }
});

export const { getInitPositionData, onGetInitPositionData } = PositionSlice.actions;

export default PositionSlice.reducer;
