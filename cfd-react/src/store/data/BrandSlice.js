import { createSlice } from '@reduxjs/toolkit';
import { encSliceName } from '../../utils/SystemUtils'

export const BrandSlice = createSlice({
  name: encSliceName('brand'),
  initialState: {
    brandList: [],
    brandKeyMapList: {}
  },
  reducers: {
    getBrandData: (state, action) => {
    },
    onGetBrandData: (state, action) => {
      state.brandList = action.payload.brandList
      state.brandKeyMapList = action.payload.brandKeyMapList
    },
  }
});

export const { getBrandData, onGetBrandData } = BrandSlice.actions;

export default BrandSlice.reducer;
