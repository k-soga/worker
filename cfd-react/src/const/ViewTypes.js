export const VIEW_TYPES = {
  MAIN: 'main',                                      //メイン画面
  NEW_ORDER: 'new_order',                            //新規注文
  SET_ORDER: 'set_order',                            //決済注文
  CORRECT_ORDER: 'correct_order',                    //注文訂正
  CANCEL_ORDER: 'cancel_order',                      //注文取消
  MULTI_SET_ORDER: 'multi_set_order',                //複数決済注文
  REF_EFFECT_ORDER: 'ref_effect_order',              //有効注文照会
  REF_NO_EFFECT_ORDER: 'ref_no_effect_order',        //無効注文照会
  REF_ORDER_HISTORY: 'ref_order_history',            //取引履歴照会
  REF_POSITION: 'ref_position',                 //ポジション照会
  REF_ACCOUNT_BALANCE: 'ref_account_balance',        //代表口座照会
  CLEAR_FORCED_SET_ORDER: 'clear_forced_set_order',  //強制決済解消
  ORDER_SETTING: 'order_setting',                    //取引設定
}
