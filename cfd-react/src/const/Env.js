export const RUN_MODES = {
  PROD: 'production',
  TEST: 'test',
  DEV: 'development',
}

export const LOG_LEVELS = {
  RUN   : 0,
  ERROR : 1,
  WARN  : 2,
  LOG   : 3,
  INFO  : 4,
  DEBUG : 5,
  TRACE  : 6,
}
