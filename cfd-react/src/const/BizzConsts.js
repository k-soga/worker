/*******************************************************
 * 汎用値
 *******************************************************/
export const SELECT_ALL = 'all'

/*******************************************************
 * CFD口座種別
 *******************************************************/
export const CFD_ACC_TYPE = {
  STOCK:   1, //株価指数
  PRODUCT: 2, //商品
}

const initCfdAccTypeName = () => {
  let _CFD_ACC_TYPE_NAME = {};
  _CFD_ACC_TYPE_NAME[CFD_ACC_TYPE.STOCK] = "株価指数"
  _CFD_ACC_TYPE_NAME[CFD_ACC_TYPE.PRODUCT] = "商品"
  return _CFD_ACC_TYPE_NAME
}

export const CFD_ACC_TYPE_NAME = initCfdAccTypeName();

const initSelectListCfdAccType = () => {
  let list = []
  list.push({'key':SELECT_ALL, 'value':'全口座'})
  for (let key in CFD_ACC_TYPE_NAME) {
    list.push({'key':key, 'value':CFD_ACC_TYPE_NAME[key]})
  }
  return list
}

export const SELECT_LIST_CFD_ACC_TYPE = initSelectListCfdAccType();

/*******************************************************
 * 売買区分
 *******************************************************/
export const SELL_BUY = {
  SELL: 1, //売
  BUY:  2, //買
}

const initSellBuyName = () => {
  let _SELL_BUY_NAME = {};
  _SELL_BUY_NAME[SELL_BUY.SELL] = "売"
  _SELL_BUY_NAME[SELL_BUY.BUY] = "買"
  return _SELL_BUY_NAME
}

export const SELL_BUY_NAME = initSellBuyName();

/*******************************************************
 * 新規決済区分
 *******************************************************/
export const NEW_SET_DIV = {
  NEW: 1,  //新規
  SET: 2,  //決済
}

const initNewSetDivName = () => {
  let _NEW_SET_DIV_NAME = {};
  _NEW_SET_DIV_NAME[NEW_SET_DIV.NEW] = "新規"
  _NEW_SET_DIV_NAME[NEW_SET_DIV.SET] = "決済"
  return _NEW_SET_DIV_NAME
}

export const NEW_SET_DIV_NAME = initNewSetDivName();

/*******************************************************
 * 注文種別
 *******************************************************/
//執行条件
export const EXEC_CONDITION = {
  NONE:         -1,
  MARKET:        1,    //成行
  POINT:         2,    //指値
  REVERSE_POINT: 3,    //逆指値
}

//注文タイプ
export const ORDER_TYPE = {
  MARKET_DISP:  'A',    //成行表示
  MARKET_AUTO:  'B',    //成行自動
  SINGLE_POINT: 'C',    //単体正逆指値
  IFD:          'D',    //IFD
  IFO:          'E',    //IFO
  N_OCO:        'F',    //N=OCO
  L_OCO:        'G',    //L-OCO  ※通常このOCOを利用
  OCO_OCO:      'H',    //OCO+OCO
  FORCED_SET:   'Y',    //強制決済
  STOP_LOSS:    'Z',    //自動ストップロス
}

const initOrderTypeName = () => {
  let _ORDER_TYPE_NAME = {};
  _ORDER_TYPE_NAME[EXEC_CONDITION.MARKET        + ':' + ORDER_TYPE.MARKET_DISP] = "成行表示"
  _ORDER_TYPE_NAME[EXEC_CONDITION.MARKET        + ':' + ORDER_TYPE.MARKET_AUTO] = "成行自動"
  _ORDER_TYPE_NAME[EXEC_CONDITION.POINT         + ':' + ORDER_TYPE.SINGLE_POINT] = "指値"
  _ORDER_TYPE_NAME[EXEC_CONDITION.REVERSE_POINT + ':' + ORDER_TYPE.SINGLE_POINT] = "逆指値"
  _ORDER_TYPE_NAME[EXEC_CONDITION.POINT         + ':' + ORDER_TYPE.IFD] = "IFD 指値"
  _ORDER_TYPE_NAME[EXEC_CONDITION.REVERSE_POINT + ':' + ORDER_TYPE.IFD] = "IFD 逆指"
  _ORDER_TYPE_NAME[EXEC_CONDITION.POINT         + ':' + ORDER_TYPE.IFO] = "IFO 指値"
  _ORDER_TYPE_NAME[EXEC_CONDITION.REVERSE_POINT + ':' + ORDER_TYPE.IFO] = "IFO 逆指"
  _ORDER_TYPE_NAME[EXEC_CONDITION.POINT         + ':' + ORDER_TYPE.L_OCO] = "OCO 指値"
  _ORDER_TYPE_NAME[EXEC_CONDITION.REVERSE_POINT + ':' + ORDER_TYPE.L_OCO] = "OCO 逆指"
  _ORDER_TYPE_NAME[EXEC_CONDITION.NONE          + ':' + ORDER_TYPE.FORCED_SET] = "強制決済"
  _ORDER_TYPE_NAME[EXEC_CONDITION.NONE          + ':' + ORDER_TYPE.STOP_LOSS] = "自動ﾛｽｶtｯﾄ"
  return _ORDER_TYPE_NAME
}

export const ORDER_TYPE_NAME = initOrderTypeName();
  
const initSetOrderTypeName = () => {
  let _SET_ORDER_TYPE_NAME = {};
  _SET_ORDER_TYPE_NAME[EXEC_CONDITION.MARKET        + ':' + ORDER_TYPE.MARKET_DISP] = "成行表示"
  _SET_ORDER_TYPE_NAME[EXEC_CONDITION.MARKET        + ':' + ORDER_TYPE.MARKET_AUTO] = "成行自動"
  _SET_ORDER_TYPE_NAME[EXEC_CONDITION.POINT         + ':' + ORDER_TYPE.SINGLE_POINT] = "指値"
  _SET_ORDER_TYPE_NAME[EXEC_CONDITION.REVERSE_POINT + ':' + ORDER_TYPE.SINGLE_POINT] = "逆指値"
  _SET_ORDER_TYPE_NAME[EXEC_CONDITION.POINT         + ':' + ORDER_TYPE.IFD] = "指値"
  _SET_ORDER_TYPE_NAME[EXEC_CONDITION.REVERSE_POINT + ':' + ORDER_TYPE.IFD] = "逆指値"
  _SET_ORDER_TYPE_NAME[EXEC_CONDITION.POINT         + ':' + ORDER_TYPE.IFO] = "OCO"
  _SET_ORDER_TYPE_NAME[EXEC_CONDITION.REVERSE_POINT + ':' + ORDER_TYPE.IFO] = "OCO"
  _SET_ORDER_TYPE_NAME[EXEC_CONDITION.POINT         + ':' + ORDER_TYPE.L_OCO] = "OCO"
  _SET_ORDER_TYPE_NAME[EXEC_CONDITION.REVERSE_POINT + ':' + ORDER_TYPE.L_OCO] = "OCO"
  _SET_ORDER_TYPE_NAME[EXEC_CONDITION.NONE          + ':' + ORDER_TYPE.FORCED_SET] = "強制決済"
  _SET_ORDER_TYPE_NAME[EXEC_CONDITION.NONE          + ':' + ORDER_TYPE.STOP_LOSS] = "自動ﾛｽｶtｯﾄ"
  return _SET_ORDER_TYPE_NAME
}

export const SET_ORDER_TYPE_NAME = initSetOrderTypeName();

/*******************************************************
 * 有効期限
 *******************************************************/
export const EFFECT_PERIOD = {
  GTC:  0,    //無期限
  WEEK: 1,    //1週間
  DAY:  2,    //1日限り
}

const initEffectPeriodName = () => {
  let _EFFECT_PERIOD_NAME = {};
  _EFFECT_PERIOD_NAME[EFFECT_PERIOD.GTC]  = "GTC"
  _EFFECT_PERIOD_NAME[EFFECT_PERIOD.WEEK] = "WEEK"
  _EFFECT_PERIOD_NAME[EFFECT_PERIOD.DAY]  = "DAY"
  return _EFFECT_PERIOD_NAME
}

export const EFFECT_PERIOD_NAME = initEffectPeriodName();