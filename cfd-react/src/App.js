import React, { useState } from 'react';
import { DndProvider } from "react-dnd";
import { HTML5Backend } from "react-dnd-html5-backend";
import _ from "lodash";
import { Board } from "./Board";

let _columnId = 0;
let _cardId = 0;

const initialCards = Array.from({ length: 4 }).map(() => ({
  id: ++_cardId,
  title: `銘柄 ${_cardId}`
}));

const initialColumns = ["表示", "非表示"].map((title, i) => ({
  id: _columnId++,
  title,
  cardIds: initialCards.slice(i * 2, i * 2 + 2).map((card) => card.id)
}));

export default function App () {
  const [state, setState] = useState({ cards: initialCards, columns: initialColumns });

  const moveCard = (cardId, destColumnId, index) => {
    setState((state) => ({
      columns: state.columns.map((column) => ({ ...column, cardIds: _.flowRight(
          (ids) =>
            column.id === destColumnId
              ? [...ids.slice(0, index), cardId, ...ids.slice(index)]
              : ids,
          (ids) => ids.filter((id) => id !== cardId)
        )(column.cardIds)
      }))
    }));
  };

  return (
    <DndProvider backend={HTML5Backend}>
      <Board
        cards={state.cards}
        columns={state.columns}
        moveCard={moveCard}
      />
    </DndProvider>
  );
}