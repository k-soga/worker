import React from 'react'
import { useSelector } from 'react-redux'
import { makeStyles } from '@material-ui/core/styles'
import Paper from '@material-ui/core/Paper'
import { getSliceVal } from '../../utils/SystemUtils'
import { getMainContentsPartsViewHeight } from '../../utils/UiUtils'

const useStyles = makeStyles({
  root: { height: '100%', background: '#d2dde8', padding: '1px 0 0 1px', },
  head: { width: 'calc(500% -15px)', height: '30px', color: '#ffffff', margin: '5px 2px 5px 0',
    backgroundColor: '#213e3b',
    backgroundImage: 'linear-gradient(to bottom, #26494a 50%, transparent 50%, transparent), linear-gradient(to right, #26494a 50%, transparent 50%, transparent)',
    backgroundSize: '10px 10px',
  },
  head_title: { margin:' 5px 0 0 15px', float: 'left', fontSize: '14px', fontWeight: '600', },
  body: { fontSize: '12px', width:'457px', maxHeight: '210px', overflowY: 'auto' },
  row: { borderTop: '1px solid #cccccc', borderLeft: '1px solid #cccccc', borderRight: '1px solid #cccccc', height: '24px', lineHeight: '24px', },
  row_d: { borderTop: '1px solid #cccccc', borderLeft: '1px solid #cccccc', height: '24px', lineHeight: '24px', },
  cell1: { float: 'left', width:'calc(25% - 1px)', borderRight: '1px solid #cccccc', borderBottom: '1px solid #cccccc', },
  cell1_e: { float: 'left', width:'calc(25% - 2px)', borderRight: '1px solid #cccccc', borderBottom: '1px solid #cccccc', },
});

export default function MainPosition() {

  //Hooks
  const classes = useStyles()

  //Slice(Redux-State)
  const windowInfo = useSelector(state => getSliceVal(state, 'appTop', 'windowInfo'));
  const maxmin = useSelector(state => getSliceVal(state, 'rateView', 'maxmin'));

  return (
    <Paper elevation={3} className={classes.root}>
      <Paper elevation={5} className={classes.head}>
        <div className={ classes.head_title }>代表口座照会</div>
      </Paper>
      <Paper elevation={5} className={classes.body} style={{ maxHeight: String(getMainContentsPartsViewHeight(maxmin, windowInfo))+'px' }}>
        <div className={ classes.row_d }>
          <div className={ classes.cell1 }>見出し</div>
          <div className={ classes.cell1 }>XX,XXX,XXX,XXX円</div>
          <div className={ classes.cell1 }>見出し</div>
          <div className={ classes.cell1_e }>XX,XXX,XXX,XXX円</div>
        </div>
        <div className={ classes.row }>
          指数
        </div>
        <div className={ classes.row_d }>
          <div className={ classes.cell1 }>見出し</div>
          <div className={ classes.cell1 }>XX,XXX,XXX,XXX円</div>
          <div className={ classes.cell1 }>見出し</div>
          <div className={ classes.cell1_e }>XX,XXX,XXX,XXX円</div>
        </div>
        <div className={ classes.row_d }>
          <div className={ classes.cell1 }>見出し</div>
          <div className={ classes.cell1 }>XX,XXX,XXX,XXX円</div>
          <div className={ classes.cell1 }>見出し</div>
          <div className={ classes.cell1_e }>XX,XXX,XXX,XXX円</div>
        </div>
        <div className={ classes.row_d }>
          <div className={ classes.cell1 }>見出し</div>
          <div className={ classes.cell1 }>XX,XXX,XXX,XXX円</div>
          <div className={ classes.cell1 }>見出し</div>
          <div className={ classes.cell1 }>XX,XXX,XXX,XXX円</div>
        </div>
        <div className={ classes.row_d }>
          <div className={ classes.cell1 }>見出し</div>
          <div className={ classes.cell1 }>XX,XXX,XXX,XXX円</div>
          <div className={ classes.cell1 }>見出し</div>
          <div className={ classes.cell1_e }>XX,XXX,XXX,XXX円</div>
        </div>
        <div className={ classes.row }>
          商品
        </div>
        <div className={ classes.row_d }>
          <div className={ classes.cell1 }>見出し</div>
          <div className={ classes.cell1 }>XX,XXX,XXX,XXX円</div>
          <div className={ classes.cell1 }>見出し</div>
          <div className={ classes.cell1_e }>XX,XXX,XXX,XXX円</div>
        </div>
        <div className={ classes.row_d }>
          <div className={ classes.cell1 }>見出し</div>
          <div className={ classes.cell1 }>XX,XXX,XXX,XXX円</div>
          <div className={ classes.cell1 }>見出し</div>
          <div className={ classes.cell1_e }>XX,XXX,XXX,XXX円</div>
        </div>
        <div className={ classes.row_d }>
          <div className={ classes.cell1 }>見出し</div>
          <div className={ classes.cell1 }>XX,XXX,XXX,XXX円</div>
          <div className={ classes.cell1 }>見出し</div>
          <div className={ classes.cell1_e }>XX,XXX,XXX,XXX円</div>
        </div>
        <div className={ classes.row_d }>
          <div className={ classes.cell1 }>見出し</div>
          <div className={ classes.cell1 }>XX,XXX,XXX,XXX円</div>
          <div className={ classes.cell1 }>見出し</div>
          <div className={ classes.cell1_e }>XX,XXX,XXX,XXX円</div>
        </div>
      </Paper>
    </Paper>
  );
}