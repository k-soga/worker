import React from 'react';
import { useSelector } from 'react-redux';
import { makeStyles, withStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import FormControl from '@material-ui/core/FormControl';
import { AppSelect, AppMenuItem } from './common/AppSelect'
import TableBody from '@material-ui/core/TableBody';
import { AppTable, AppTableRow, AppTableCell } from './common/AppTable'
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import { getSliceVal } from '../../utils/SystemUtils'
import { encDispDate2YearMonthDay, encDispDate2HourTime } from '../../utils/DateUtils'
import { SELECT_ALL } from '../../const/BizzConsts'
import { 
  getDispOrderNo,
  getBrandName,
  getCfdAccName,
  getSellBuyName,
  getEncodedAmount,
  getNewOrderTypeName,
  encDigit,
  getEffectPeriodName,
  getConditioningNewOrderList,
} from '../../utils/BizzUtils'
import { getMainContentsPartsViewHeight } from '../../utils/UiUtils'

export const OrderTableCell = withStyles((theme) => ({
  root: {
    padding: '0 5px',
    borderRight: '1px solid #cccccc99',
    fontSize: '11px',
    fontFamily: '"Hiragino Kaku Gothic ProN","メイリオ", sans-serif',
    lineHeight: '20px',
  },
  stickyHeader: {
    background: '#4ea1a9ee', 
    backgroundImage: 'linear-gradient(to bottom, #4ea1a9ee 50%, transparent 50%, transparent), linear-gradient(to right, #4ea1a9ee 50%, transparent 50%, transparent)',
    backgroundSize: '5px 5px',
    color: '#ffffff',
    lineHeight: '16px',
  },
}))(AppTableCell);

const useStyles = makeStyles({
  root: { height: '100%', background: '#d2dde8', padding: '1px 0 0 1px', },
  head: { width: 'calc(100% -15px)', height: '30px', color: '#ffffff', margin: '5px 2px 5px 0',
    backgroundColor: '#213e3b',
    backgroundImage: 'linear-gradient(to bottom, #26494a 50%, transparent 50%, transparent), linear-gradient(to right, #26494a 50%, transparent 50%, transparent)',
    backgroundSize: '10px 10px',
  },
  head_title: { margin:' 5px 0 0 15px', float: 'left', fontSize: '14px', fontWeight: '600', },
  select_label_cfd_acc: { margin:'8px 0 0 20px', float: 'left', fontSize: '11px' },
  form_control_cfd_acc: { width: '80px' },
  select_area_cfd_acc: { margin:'5px 0 0 10px', float: 'left' },
  select_label_brand: { margin:'8px 0 0 10px', float: 'left', fontSize: '11px' },
  form_control_brand: { width: '130px' },
  select_area_brand: { margin:'5px 0 0 10px', float: 'left' },
  // body: { width: 'calc(100% - 3px)' }, 
  body: { minWidth: '618px', maxWidth: '890px', },
  container: { minWidth: '618px', maxWidth: '890px', maxHeight: '210px', },
  table: { background: '#ffffff' },
  head_row: {  height: '12px', },
  cell_order_no: { minWidth: '90px', maxWidth: '100px' }, 
  cell_brand_name: { minWidth: '70px', maxWidth: '100px' }, 
  cell_cfd_acc_name: { minWidth: '50px', maxWidth: '60px' }, 
  cell_sell_buy_div: { minWidth: '12px', maxWidth: '40px' }, 
  cell_amount: { minWidth: '50px', maxWidth: '60px' }, 
  cell_order_type: { minWidth: '50px', maxWidth: '70px' }, 
  cell_point_price: { minWidth: '50px', maxWidth: '70px' }, 
  cell_effective_period: { minWidth: '46px', maxWidth: '60px' }, 
  cell_order_date: { minWidth: '100px', maxWidth: '120px' }, 
  body_row: {  height: '12px', },
  contract_date: { width: '105px' },
  set_order: { height: '12px', borderBottom: '1px solid #cccccc99', },
  set_order_type: { width:'50px', height: '12px', float: 'left', borderRight: '1px solid #cccccc99', },
  set_order_amount: { width:'50px', height: '12px', float: 'left', borderRight: '1px solid #cccccc99', },
  set_order_point: { width:'50px', height: '12px', float: 'left', borderRight: '1px solid #cccccc99', },
  set_order_reverse_point: { width:'50px', height: '12px', float: 'left', borderRight: '1px solid #cccccc99', },
  set_order_command: { width:'109px', height: '12px', float: 'left', },
  set_order_type_value: { width:'40px', },
  set_order_amount_value: { width:'40px', },
  set_order_point_value: { width:'40px', },
  set_order_reverse_point_value: { width:'40px', },
  set_order_command_value: { width:'100px', },
});

export default function MainNewEffectOrder() {
  const classes = useStyles();

  //Slice(Redux-State)
  const windowInfo = useSelector(state => getSliceVal(state, 'appTop', 'windowInfo'));
  const _newOrderList = useSelector(state => getSliceVal(state, 'effectOrder', 'newOrderList'));
  const maxmin = useSelector(state => getSliceVal(state, 'rateView', 'maxmin'));
  const newOrderSelectCdfAccList = useSelector(state => getSliceVal(state, 'effectOrder', 'newOrderSelectCdfAccList'));
  const newOrderSelectBrandList = useSelector(state => getSliceVal(state, 'effectOrder', 'newOrderSelectBrandList'));
  const brandKeyMapList = useSelector(state => getSliceVal(state, 'brand', 'brandKeyMapList'));


  //Component-State
  const [selectedCfdAcc, setSelectedCfdAcc] = React.useState(SELECT_ALL);
  const [selectedBrand, setSelectedBrand] = React.useState(SELECT_ALL);

  const newOrderList = getConditioningNewOrderList(_newOrderList, selectedCfdAcc, selectedBrand)

  const handleChangeCfdAcc = (event) => {
    setSelectedCfdAcc(event.target.value);
  };

  const handleChangeBrand = (event) => {
    setSelectedBrand(event.target.value);
  }

  return (
    <Paper elevation={3} className={classes.root}>
      <Paper elevation={5} className={classes.head}>
        <div className={ classes.head_title }>新規注文状況</div>
        <div className={classes.select_label_cfd_acc}>
          検索：口座
        </div>
        
        <div className={classes.select_area_cfd_acc}>
          <FormControl variant="outlined" size="small" className={classes.form_control_cfd_acc}>
            <AppSelect value={selectedCfdAcc} onChange={handleChangeCfdAcc}>
              {newOrderSelectCdfAccList.map((row) => (
                <AppMenuItem key={row.key} value={row.key}>{row.value}</AppMenuItem>
              ))}
            </AppSelect>
          </FormControl>
        </div>
        
        <div className={classes.select_label_brand}>
          検索：銘柄
        </div>
        <div className={classes.select_area_brand}>
          <FormControl variant="outlined" size="small" className={classes.form_control_brand}>
            <AppSelect value={selectedBrand} onChange={handleChangeBrand}>
              {newOrderSelectBrandList.map((row) => (
                <AppMenuItem key={row.key} value={row.key}>{row.value}</AppMenuItem>
              ))}
            </AppSelect>
          </FormControl>
        </div>
      </Paper>
      <Paper elevation={5} className={classes.body}>
        <TableContainer className={classes.container} style={{ maxHeight: String(getMainContentsPartsViewHeight(maxmin, windowInfo))+'px' }}>
          <AppTable stickyHeader className={classes.table}>
            <TableHead>
              <TableRow className={classes.head_row}>
                <OrderTableCell align="center" className={classes.cell_order_no}>注文No.</OrderTableCell>
                <OrderTableCell align="center" className={classes.cell_brand_name}>銘柄</OrderTableCell>
                <OrderTableCell align="center" className={classes.cell_cfd_acc_name}>口座</OrderTableCell>
                <OrderTableCell align="center" className={classes.cell_sell_buy_div}>
                  <div style={{ verticalAlign: 'bottom', lineHeight: '10px', marginTop: '4px', }}>売</div>
                  <div>買</div>
                </OrderTableCell>
                <OrderTableCell align="center" className={classes.cell_amount}>数量</OrderTableCell>
                <OrderTableCell align="center" className={classes.cell_order_type}>種類</OrderTableCell>
                <OrderTableCell align="center" className={classes.cell_point_price}>指定値段</OrderTableCell>
                <OrderTableCell align="center" className={classes.cell_effective_period}>有効期限</OrderTableCell>
                <OrderTableCell align="center" className={classes.cell_order_date}>受注日時</OrderTableCell>
              </TableRow>
            </TableHead>
            <TableBody>
            {newOrderList.map((row) => (
              <AppTableRow key={getDispOrderNo(row.orderNo, row.orderSubNo)} className={classes.head_row}>
                <OrderTableCell align="center">{ getDispOrderNo(row.orderNo, row.orderSubNo) }</OrderTableCell>
                {/* 銘柄 */}
                <OrderTableCell align="center">{ getBrandName(row.brandCd, brandKeyMapList) }</OrderTableCell>
                {/* 口座 */}
                <OrderTableCell align="center">{ getCfdAccName(row.cfdAccType) }</OrderTableCell>
                {/* 売買 */}
                <OrderTableCell align="center">{ getSellBuyName(row.sellBuyDiv) }</OrderTableCell>
                {/* 取引数量(万) */}
                <OrderTableCell align="right">{ getEncodedAmount(row.amount, row.brandCd, brandKeyMapList) }</OrderTableCell>
                {/* 種類 */}
                <OrderTableCell align="center">{ getNewOrderTypeName(row.execCondition, row.orderType) }</OrderTableCell>
                {/* 指定値段 */}
                <OrderTableCell align="right">{ encDigit(row.pointPrice, row.brandCd, brandKeyMapList) }</OrderTableCell>
                {/* 有効期限 */}
                <OrderTableCell align="center">{ getEffectPeriodName(row.effectivePeriod) }</OrderTableCell>
                {/* 受注日時 */}
                <OrderTableCell align="center">{ encDispDate2YearMonthDay(row.orderDate) }&nbsp;{ encDispDate2HourTime(row.orderDate) }</OrderTableCell>
              </AppTableRow>
            ))}
            </TableBody>
          </AppTable>
        </TableContainer>
      </Paper>
    </Paper>
  );
}