import React from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { makeStyles } from '@material-ui/core/styles'
import Paper from '@material-ui/core/Paper'
import FormControl from '@material-ui/core/FormControl';
import { AppSelect, AppMenuItem } from './common/AppSelect'
import ToggleButtonGroup from '@material-ui/lab/ToggleButtonGroup'
import Reorder from '@material-ui/icons/Reorder'
import ViewModule from '@material-ui/icons/ViewModule'
import MainPositionList from './element/MainPositionList'
import MainPositionSum from './element/MainPositionSum'
import MainPositionToggleButton from './element/MainPositionToggleButton'
import { getSliceVal } from '../../utils/SystemUtils'
import { MAIN_POSITION_MODE, onChangeViewMode } from '../../store/ui/PositionViewSlice'
import { SELECT_ALL } from '../../const/BizzConsts'

const useStyles = makeStyles({
  root: { height: 'calc(100% - 3px)', background: '#d2dde8', overflow: 'hidden' },
  head: { width: 'calc(100% -15px)', height: '30px', color: '#ffffff', margin: '5px 2px 5px 0',
    backgroundColor: '#213e3b',
    backgroundImage: 'linear-gradient(to bottom, #26494a 50%, transparent 50%, transparent), linear-gradient(to right, #26494a 50%, transparent 50%, transparent)',
    backgroundSize: '10px 10px',
  },
  head_title: { margin:' 5px 0 0 15px', float: 'left', fontSize: '14px', fontWeight: '600', },
  btn_panel: { height: '12px', fontSize: '11px', margin: '5px 2px 5px 15px', padding: '10px 5px 10px 2px', },
  btn_list: { height: '12px', fontSize: '11px', margin: '5px 2px 5px 15px', padding: '10px 5px 10px 2px', },
  select_label_cfd_acc: { margin:'8px 0 0 20px', float: 'left', fontSize: '11px' },
  form_control_cfd_acc: { width: '80px' },
  select_area_cfd_acc: { margin:'5px 0 0 10px', float: 'left' },
  select_label_brand: { margin:'8px 0 0 10px', float: 'left', fontSize: '11px' },
  form_control_brand: { width: '130px' },
  select_area_brand: { margin:'5px 0 0 10px', float: 'left' },
});

export default function MainPosition() {

  //Hooks
  const classes = useStyles()
  const dispatch = useDispatch()

  //Slice(Redux-State)
  const viewMode = useSelector(state => getSliceVal(state, 'positionView', 'viewMode'));
  const _positionSelectCdfAccList = useSelector(state => getSliceVal(state, 'position', 'positionSelectCdfAccList'));
  const _positionSelectBrandList = useSelector(state => getSliceVal(state, 'position', 'positionSelectBrandList'));
  const positionSelectCdfAccList = _positionSelectCdfAccList !== undefined ? _positionSelectCdfAccList : []
  const positionSelectBrandList = _positionSelectBrandList !== undefined ? _positionSelectBrandList : []
  
  //Component-State
  const [selectedCfdAcc, setSelectedCfdAcc] = React.useState(SELECT_ALL);
  const [selectedBrand, setSelectedBrand] = React.useState(SELECT_ALL);

  const handleClickViewMode = (event, newMode) => {
    if(newMode !== null && viewMode !== newMode) {
      dispatch(onChangeViewMode(newMode))
    }
  }

  const handleChangeCfdAcc = (event) => {
    setSelectedCfdAcc(event.target.value);
  };

  const handleChangeBrand = (event) => {
    setSelectedBrand(event.target.value);
  }

  return (
    <Paper elevation={3} className={classes.root}>
      <Paper elevation={5} className={classes.head}>
        <div className={ classes.head_title }>ポジション状況</div>
        <div style={{ marginLeft:'10px', float: 'left' }}>
          <ToggleButtonGroup value={viewMode} exclusive onChange={handleClickViewMode} aria-label="text alignment">
            <MainPositionToggleButton value={MAIN_POSITION_MODE.LIST} aria-label="centered" className={classes.btn_list}>
              <Reorder />詳細
            </MainPositionToggleButton>
            <MainPositionToggleButton value={MAIN_POSITION_MODE.SUMMARY} aria-label="left aligned" className={classes.btn_panel}>
              <ViewModule />サマリ
            </MainPositionToggleButton>
          </ToggleButtonGroup>
        </div>
        <div className={classes.select_label_cfd_acc}>
          検索：口座
        </div>
        <div className={classes.select_area_cfd_acc}>
          <FormControl variant="outlined" size="small" className={classes.form_control_cfd_acc}>
            <AppSelect value={selectedCfdAcc} onChange={handleChangeCfdAcc}>
              {positionSelectCdfAccList.map((row) => (
                <AppMenuItem key={row.key} value={row.key}>{row.value}</AppMenuItem>
              ))}
            </AppSelect>
          </FormControl>
        </div>
        <div className={classes.select_label_brand}>
          検索：銘柄
        </div>
        <div className={classes.select_area_brand}>
          <FormControl variant="outlined" size="small" className={classes.form_control_brand}>
            <AppSelect value={selectedBrand} onChange={handleChangeBrand}>
              {positionSelectBrandList.map((row) => (
                <AppMenuItem key={row.key} value={row.key}>{row.value}</AppMenuItem>
              ))}
            </AppSelect>
          </FormControl>
        </div>
      </Paper>
      { viewMode !== MAIN_POSITION_MODE.SUMMARY && (
        <MainPositionList selectedCfdAcc={selectedCfdAcc} selectedBrand={selectedBrand} />
      )}
      { viewMode === MAIN_POSITION_MODE.SUMMARY && (
        <MainPositionSum selectedCfdAcc={selectedCfdAcc} selectedBrand={selectedBrand} />
      )}
    </Paper>
  );
}