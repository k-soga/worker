import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';

const useStyles = makeStyles((theme) => ({
  root: {
    height: 'calc(100% - 12px)',
    padding: '5px',
    overflow: 'auto'
  }
}));


export default function OrderCorrect() {
  const classes = useStyles();
  return (
    <Paper elevation={3} className={classes.root}>
      注文訂正
    </Paper>
  );
}
