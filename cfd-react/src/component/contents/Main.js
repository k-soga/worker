import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import MainPosition from './MainPosition'
import MainAccount from './MainAccount'
import MainNewEffectOrder from './MainNewEffectOrder'

const useStyles = makeStyles({
  root: { width: '100%', height: '100%' },
  position: { width: '100%', minWidth:'986px', height: '50%', margin: '0 auto', overflowX: 'auto' },
  account_and_order: {width: '100%', minWidth:'986px', height: '50%', margin: '0 auto', },
  account: { float: 'left', width: '460px', height: '100%', },
  new_effect_order: { float: 'left', width:'calc(100% - 463px)', minWidth: '523px', height: '100%', marginLeft: '3px',},
});

export default function Main(props) {
  const classes = useStyles();

  return (
    <div className={classes.root}>
      <div className={classes.position}>
        <MainPosition/>
      </div>
      <div className={classes.account_and_order}>
        <div className={classes.account}>
          <MainAccount/>
        </div>
        <div className={classes.new_effect_order}>
          <MainNewEffectOrder/>
        </div>
      </div>
    </div>
  );
}