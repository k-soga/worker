import React from 'react';
import RatePanelRow from './RatePanelRow';

export default function RatePanel(props) {

  //PropsParam
  const currRateList = props.currRateList
  const brandKeyMapList = props.brandKeyMapList ? props.brandKeyMapList : []
  
  return (
    <div>
      {currRateList.map((row) => (
        <RatePanelRow key={row.brandCd} rate={row} brand={brandKeyMapList[row.brandCd]} />
      ))}
    </div>
  );
}
