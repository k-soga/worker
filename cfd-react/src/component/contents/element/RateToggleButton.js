import { withStyles } from '@material-ui/core/styles';
import ToggleButton from '@material-ui/lab/ToggleButton';

const RateToggleButton = withStyles((theme) => ({
  root: {
    color: '#000000',
    backgroundColor: '#e0e0e0',
    '&:hover': {
      color: '#000000',
      backgroundColor: '#e0e0e0',
    }
  },
  selected: {
    '&.Mui-selected': {
      color: '#000000',
      backgroundColor: '#fbfa45ee',
      '&:hover': {
        color: '#000000',
        backgroundColor: '#fbfa45ee',
      }
    }
  },
}))(ToggleButton);

export default RateToggleButton