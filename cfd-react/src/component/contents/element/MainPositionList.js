import React from 'react';
import { useSelector } from 'react-redux';
import { makeStyles, withStyles } from '@material-ui/core/styles';
import clsx from 'clsx';
import Button from '@material-ui/core/Button';
import TableBody from '@material-ui/core/TableBody';
import { AppTable, AppTableRow, AppTableFootRow, AppTableCell } from '../common/AppTable'
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import { getSliceVal } from '../../../utils/SystemUtils'
import { encDispDate } from '../../../utils/DateUtils'
import { 
  getBrandName, 
  getCfdAccName, 
  getSellBuyName, 
  getEncodedAmount, 
  encDigit, 
  calcTotalProfit,
  getSetOrderTypeName,
  getConditioningPositionList
} from '../../../utils/BizzUtils'
import { getMainContentsPartsViewHeight } from '../../../utils/UiUtils'

export const PositionTableCell = withStyles((theme) => ({
  root: {
    padding: '0 5px',
    borderRight: '1px solid #cccccc99',
    fontSize: '11px',
    fontFamily: '"Hiragino Kaku Gothic ProN","メイリオ", sans-serif',
    lineHeight: '20px',
  },
  stickyHeader: {
    background: '#4ea1a9ee', 
    backgroundImage: 'linear-gradient(to bottom, #4ea1a9ee 50%, transparent 50%, transparent), linear-gradient(to right, #4ea1a9ee 50%, transparent 50%, transparent)',
    backgroundSize: '5px 5px',
    color: '#ffffff',
    lineHeight: '16px',
  },
}))(AppTableCell);

export const OrderCommandButton = withStyles((theme) => ({
  root: {
    width: '30px',
    minWidth: '10px',
    padding: '0',
    borderRight: '1px solid #cccccc99',
    fontSize: '10px',
    fontFamily: '"Hiragino Kaku Gothic ProN","メイリオ", sans-serif',
  },
}))(Button);

const useStyles = makeStyles({
  // root: { height: 'calc(100% - 12px)', background: '#d2dde8', padding: '1px 0 0 1px', },
  root: { minWidth: '1014px', height: 'calc(100% - 12px)', background: '#d2dde8', padding: '1px 0 0 1px', },
  // container: { width: 'calc(100% - 2px)', maxHeight: '210px', },
  container: { minWidth: '1163px', maxWidth: '1371px', maxHeight: '210px', },
  // head: { width: 'calc(100% -15px)', height: '30px', color: '#ffffff', margin: '5px 2px 5px 0',
  head: { minWidth: '1163px', maxWidth: '1371px', height: '30px', color: '#ffffff', margin: '5px 2px 5px 0',
    backgroundColor: '#213e3b',
    backgroundImage: 'linear-gradient(to bottom, #26494a 50%, transparent 50%, transparent), linear-gradient(to right, #26494a 50%, transparent 50%, transparent)',
    backgroundSize: '10px 10px',
  },
  head_title: { margin:' 5px 0 0 15px', float: 'left', fontSize: '14px', fontWeight: '600', },
  table: { background: '#ffffff' },
  head_row: {  height: '12px', },
  body_row: {  height: '12px', },
  foot_row: {  height: '24px', verticalAlign: 'bottom' },
  minus_value: { color: '#ff0000'},
  cell_brand_name: { minWidth: '70px', maxWidth: '100px' }, 
  cell_cfd_acc_name: { minWidth: '50px', maxWidth: '60px' }, 
  cell_sell_buy_div: { minWidth: '12px', maxWidth: '40px' }, 
  cell_contract_date: { minWidth: '100px', maxWidth: '120px' }, 
  cell_amount: { minWidth: '50px', maxWidth: '60px' }, 
  cell_contract_rate: { minWidth: '50px', maxWidth: '70px' }, 
  cell_current_rate: { minWidth: '50px', maxWidth: '70px' }, 
  cell_pips: { minWidth: '30px', maxWidth: '70px' }, 
  cell_interest_adjustment: { minWidth: '90px', maxWidth: '100px' }, 
  cell_price_adjustment: { minWidth: '90px', maxWidth: '100px' }, 
  cell_profit: { minWidth: '90px', maxWidth: '100px' }, 
  cell_set_order_block: {padding: '0', width: '292px'},
  cell_set_order: { height: '12px', borderBottom: '1px solid #cccccc99', },
  cell_set_order_type: { width:'46px', height: '12px', float: 'left', borderRight: '1px solid #cccccc99', },
  cell_set_order_amount: { width:'46px', height: '12px', float: 'left', borderRight: '1px solid #cccccc99', },
  cell_set_order_point: { width:'50px', height: '12px', float: 'left', borderRight: '1px solid #cccccc99', },
  cell_set_order_reverse_point: { width:'50px', height: '12px', float: 'left', borderRight: '1px solid #cccccc99', },
  cell_set_order_command: { width:'112px', height: '12px', float: 'left', },
  set_order_none_value: { textAlign: 'center', },
  set_order_type_value: { width:'36px', },
  set_order_amount_value: { width:'36px', },
  set_order_point_value: { width:'40px', },
  set_order_reverse_point_value: { width:'40px', },
  set_order_command_value: { width:'103px', },
  btn_set_order: { float: 'left', marginRight: '3px', },
  btn_correct_order: { float: 'left', marginRight: '3px', },
  btn_cancel_order: { float: 'left', },
});

export default function MainPositionList(props) {
  const classes = useStyles();

  //PropsParam
  const selectedCfdAcc = props.selectedCfdAcc
  const selectedBrand = props.selectedBrand

  //Slice(Redux-State)
  const windowInfo = useSelector(state => getSliceVal(state, 'appTop', 'windowInfo'));
  const maxmin = useSelector(state => getSliceVal(state, 'rateView', 'maxmin'));
  const _positionList = useSelector(state => getSliceVal(state, 'position', 'positionList'));
  const _positionInSetOrderList = useSelector(state => getSliceVal(state, 'position', 'positionInSetOrderList'));
  const brandKeyMapList = useSelector(state => getSliceVal(state, 'brand', 'brandKeyMapList'));

  const positionList = getConditioningPositionList(_positionList, selectedCfdAcc, selectedBrand)
  const positionInSetOrderList = getConditioningPositionList(_positionInSetOrderList, selectedCfdAcc, selectedBrand)
  
  const totalProfit = calcTotalProfit(positionList)   //評価損益合計

  //描画値取得：銘柄
  const drawBrandName = (row) => {
    return row.isDraw ? getBrandName(row.brandCd, brandKeyMapList) : ''
    
  }
  //描画値取得：CFD口座種別
  const drawCfdAccName = (row) => {
    return row.isDraw ? getCfdAccName(row.cfdAccType) : ''
  }
  //描画値取得：売買区分
  const drawSellBuyName = (row) => {
    return row.isDraw ? getSellBuyName(row.sellBuyDiv) : ''
  }
  //描画値取得：成立値段
  const drawContractDate = (row) => {
    return row.isDraw ? encDispDate(row.contractDate) : ''
  }
  //描画値取得：取引数量
  const drawAmount = (row) => {
    return row.isDraw ? getEncodedAmount(row.amount, row.brandCd, brandKeyMapList) : ''
  }
  //描画値取得：成立値段
  const drawContractRate = (row) => {
    return row.isDraw ? encDigit(row.contractRate, row.brandCd, brandKeyMapList) : ''
  }
  //描画値取得：現在値段
  const drawCurrRate = (row) => {
    return row.isDraw ? encDigit(row.currRate, row.brandCd, brandKeyMapList) : ''
  }
  //描画値取得：pips
  const drawPips = (row) => {
    return row.isDraw ? encDigit(row.pips, row.brandCd, brandKeyMapList) : ''
  }
  //描画値取得：評価損益
  const drawProfit = (row) => {
    if(row.isDraw){
      if(row.profit) {
        return (row.profit).toLocaleString() + ' 円'
      } else {
        return '0 円'
      }
    } else {
      return ''
    }
  }
  //マイナス判定：pips
  const isMinusPips = (row) => {
    return row.pips < 0
  }
  //マイナス判定：評価損益
  const isMinusProfit = (row) => {
    return row.profit < 0
  }
  //描画判定：決済有効注文全般
  const isDrawSetOrder = (row) => {
    return row.setOrder.amount !== undefined;
  }
  //描画値取得：決済有効注文 - 種別
  const drawSetOrderTypeName = (row) => {
    const amount = row.setOrder.amount;
    return amount ? getSetOrderTypeName(row.setOrder.execCondition, row.setOrder.orderType) : '‐'
  }
  //描画値取得：決済有効注文 - 数量
  const drawSetOrderAmount = (row) => {
    const amount = row.setOrder.amount;
    return amount ? getEncodedAmount(amount, row.brandCd, brandKeyMapList) : '‐'
  }
  //描画判定：決済有効注文 - 指値指定値段
  const isDrawSetOrderPointValue = (row) => {
    return isDrawSetOrder(row) && row.setOrder.pointPrice !== undefined;
  }
  //描画値取得：決済有効注文 - 指値指定値段
  const drawSetOrderPointValue = (row) => {
    const pointPrice = row.setOrder.pointPrice;
    return pointPrice ? encDigit(pointPrice, row.brandCd, brandKeyMapList) : '‐'
  }
  //描画判定：決済有効注文 - 逆指値指定値段
  const isDrawSetOrderReversePointValue = (row) => {
    return isDrawSetOrder(row) && row.setOrder.revPointPrice !== undefined;
  }
  //描画値取得：決済有効注文 - 逆指値指定値段
  const drawSetOrderReversePointValue = (row) => {
    const revPointPrice = row.setOrder.revPointPrice;
    return revPointPrice ? encDigit(revPointPrice, row.brandCd, brandKeyMapList) : '‐'
  }
  //活性判定：決済注文ボタン
  const isEnabledBtnSetOrder = (row) => {
    //決済注文可能残数量がある場合は活性
    return row.remainingAmount!==undefined && row.remainingAmount > 0
  }
  //活性判定：注文訂正ボタン
  const isEnabledBtnCorrectOrder = (row) => {
    //決済有効注文がある場合は活性
    return row.setOrder.amount!==undefined && row.setOrder.amount > 0
  }
  //活性判定：注文取消ボタン
  const isEnabledBtnCancelOrder = (row) => {
    //決済有効注文がある場合は活性
    return row.setOrder.amount!==undefined && row.setOrder.amount > 0
  }
  //マイナス判定：評価損益合計
  const isMinusTotalProfit = () => {
    return totalProfit < 0
  }

  return (
    <TableContainer className={classes.container} style={{ maxHeight: String(getMainContentsPartsViewHeight(maxmin, windowInfo))+'px' }}>
      <AppTable stickyHeader className={classes.table}>
        <TableHead>
          <TableRow className={classes.head_row}>
            <PositionTableCell align="center" className={classes.cell_brand_name}>銘柄</PositionTableCell>
            <PositionTableCell align="center" className={classes.cell_cfd_acc_name}>口座</PositionTableCell>
            <PositionTableCell align="center" className={classes.cell_sell_buy_div}>
              <div style={{ verticalAlign: 'bottom', lineHeight: '10px', marginTop: '4px', }}>売</div>
              <div>買</div>
            </PositionTableCell>
            <PositionTableCell align="center" className={classes.cell_contract_date}>成立日時</PositionTableCell>
            <PositionTableCell align="center" className={classes.cell_amount}>数量</PositionTableCell>
            <PositionTableCell align="center" className={classes.cell_contract_rate}>成立値段</PositionTableCell>
            <PositionTableCell align="center" className={classes.cell_current_rate}>現在値</PositionTableCell>
            <PositionTableCell align="center" className={classes.cell_pips}>pips</PositionTableCell>
            <PositionTableCell align="center" className={classes.cell_interest_adjustment}>金利調整額</PositionTableCell>
            <PositionTableCell align="center" className={classes.cell_price_adjustment}>価格調整額</PositionTableCell>
            <PositionTableCell align="center" className={classes.cell_profit}>評価損益</PositionTableCell>
            <PositionTableCell align="center" className={classes.cell_set_order_block} colSpan={5}>
              <div className={classes.cell_set_order}>
                決済有効注文
              </div>
              <div>
                <div className={classes.cell_set_order_type}>種類</div>
                <div className={classes.cell_set_order_amount}>数量</div>
                <div className={classes.cell_set_order_point}>指値</div>
                <div className={classes.cell_set_order_reverse_point}>逆指値</div>
                <div className={classes.cell_set_order_command}>決済・訂正・取消</div>
              </div>
            </PositionTableCell>
          </TableRow>
        </TableHead>
        <TableBody>
        {positionInSetOrderList.map((row) => (
          <AppTableRow key={row._key} className={classes.body_row}>
            {/* 銘柄 */}
            <PositionTableCell align="center">{ drawBrandName(row) }</PositionTableCell>
            {/* 口座 */}
            <PositionTableCell align="center">{ drawCfdAccName(row) }</PositionTableCell>
            {/* 売買 */}
            <PositionTableCell align="center">{ drawSellBuyName(row) }</PositionTableCell>
            {/* 成立日時 */}
            <PositionTableCell align="center">{ drawContractDate(row) }</PositionTableCell>
            {/* 取引数量(万) */}
            <PositionTableCell align="right">{ drawAmount(row) }</PositionTableCell>
            {/* 成立値段 */}
            <PositionTableCell align="right">{ drawContractRate(row) }</PositionTableCell>
            {/* 現在値 */}
            <PositionTableCell align="right">{ drawCurrRate(row) }</PositionTableCell>
            {/* pips */}
            <PositionTableCell align="right" className={clsx({ [classes.minus_value]: isMinusPips(row), })}>
              { drawPips(row) }
            </PositionTableCell>
            {/* TODO 金利調整額 */}
            <PositionTableCell align="right" className={clsx({ [classes.minus_value]: isMinusProfit(row), })}>
              -999,999,999 円
            </PositionTableCell>
            {/* TODO 価格調整額 */}
            <PositionTableCell align="right" className={clsx({ [classes.minus_value]: isMinusProfit(row), })}>
             -999,999,999 円
            </PositionTableCell>
            {/* 評価損益 */}
            <PositionTableCell align="right" className={clsx({ [classes.minus_value]: isMinusProfit(row), })}>
              { drawProfit(row) }
            </PositionTableCell>
            {/* 決有効注文 - 種類 */}
            <PositionTableCell align="center" className={ classes.set_order_type_value }>
            { drawSetOrderTypeName(row) }
            </PositionTableCell>
            {/* 決有効注文 - 数量 */}
            <PositionTableCell align="right" className={clsx(classes.set_order_amount_value, { [classes.set_order_none_value]: !isDrawSetOrder(row), })}>
              { drawSetOrderAmount(row) }
            </PositionTableCell>
            {/* 決有効注文 - 指値 */}
            <PositionTableCell align="right" className={clsx(classes.set_order_point_value, { [classes.set_order_none_value]: !isDrawSetOrderPointValue(row), })}>
              { drawSetOrderPointValue(row) }
            </PositionTableCell>
            {/* 決有効注文 - 逆指値 */}
            <PositionTableCell align="right" className={clsx(classes.set_order_reverse_point_value, { [classes.set_order_none_value]: !isDrawSetOrderReversePointValue(row), })}>
              { drawSetOrderReversePointValue(row) }
            </PositionTableCell>
            <PositionTableCell align="center" className={ classes.set_order_command_value }>
              <div className={classes.btn_set_order}>
                <OrderCommandButton size="small" variant="contained" disabled={!isEnabledBtnSetOrder(row)}>
                  決済
                </OrderCommandButton>
              </div>
              <div className={classes.btn_correct_order}>
                <OrderCommandButton size="small" variant="contained" disabled={!isEnabledBtnCorrectOrder(row)}>
                  訂正
                </OrderCommandButton>
              </div>
              <div className={classes.btn_cancel_order}>
                <OrderCommandButton size="small" variant="contained" disabled={!isEnabledBtnCancelOrder(row)}>
                  取消
                </OrderCommandButton>
              </div>
            </PositionTableCell>
          </AppTableRow>
        ))}
        { positionList && positionList.length > 0 && (
          <AppTableFootRow className={classes.foot_row}>
            <PositionTableCell align="center" colSpan={3} style={{ borderRight:'none', }}></PositionTableCell>
            <PositionTableCell align="left" colSpan={5} style={{ borderRight:'none', }}>【合計】</PositionTableCell>
            <PositionTableCell align="right" style={{ borderRight:'none', }} className={clsx({ [classes.minus_value]: isMinusTotalProfit(), })}>
              { totalProfit.toLocaleString() } 円</PositionTableCell>
            <PositionTableCell align="right" style={{ borderRight:'none', }} className={clsx({ [classes.minus_value]: isMinusTotalProfit(), })}>
              { totalProfit.toLocaleString() } 円</PositionTableCell>
            <PositionTableCell align="right" style={{ borderRight:'none', }} className={clsx({ [classes.minus_value]: isMinusTotalProfit(), })}>
              { totalProfit.toLocaleString() } 円</PositionTableCell>
            <PositionTableCell align="center" colSpan={5}></PositionTableCell>
          </AppTableFootRow>
        )}
        </TableBody>
      </AppTable>
    </TableContainer>
  );
}