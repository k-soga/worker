import React from 'react';
import { useSelector } from 'react-redux';
import { makeStyles, withStyles } from '@material-ui/core/styles';
import clsx from 'clsx';
import Button from '@material-ui/core/Button';
import TableBody from '@material-ui/core/TableBody';
import { AppTable, AppTableRow, AppTableFootRow, AppTableCell } from '../common/AppTable'
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import { getSliceVal } from '../../../utils/SystemUtils'
import { encDispDate } from '../../../utils/DateUtils'
import { 
  getBrandName, 
  wrapGetCfdAccName, 
  getSellBuyName, 
  getEncodedAmount, 
  encDigit, 
  calcTotalProfit,
  getConditioningPositionList
} from '../../../utils/BizzUtils'
import { getMainContentsPartsViewHeight } from '../../../utils/UiUtils'

export const PositionTableCell = withStyles((theme) => ({
  root: {
    padding: '0 5px',
    borderRight: '1px solid #cccccc99',
    fontSize: '11px',
    fontFamily: '"Hiragino Kaku Gothic ProN","メイリオ", sans-serif',
    lineHeight: '20px',
  },
  stickyHeader: {
    background: '#4ea1a9ee', 
    backgroundImage: 'linear-gradient(to bottom, #4ea1a9ee 50%, transparent 50%, transparent), linear-gradient(to right, #4ea1a9ee 50%, transparent 50%, transparent)',
    backgroundSize: '5px 5px',
    color: '#ffffff',
    lineHeight: '16px',
  },
}))(AppTableCell);

export const OrderCommandButton = withStyles((theme) => ({
  root: {
    width: '100px',
    minWidth: '10px',
    padding: '0',
    borderRight: '1px solid #cccccc99',
    fontSize: '10px',
    fontFamily: '"Hiragino Kaku Gothic ProN","メイリオ", sans-serif',
  },
  contained: {
    background: '#ffbebe',
    '&:hover': {
      background: '#ffbebeCC',
    }
  }
}))(Button);

const useStyles = makeStyles({
  root: { height: 'calc(100% - 12px)', background: '#d2dde8', padding: '1px 0 0 1px', },
  // container: { width: 'calc(100% - 2px)', maxHeight: '210px', },
  container: { minWidth: '940px', maxWidth: '1153px', maxHeight: '210px', },
  head: { width: 'calc(100% -15px)', height: '30px', color: '#ffffff', margin: '5px 2px 5px 0',
    backgroundColor: '#213e3b',
    backgroundImage: 'linear-gradient(to bottom, #26494a 50%, transparent 50%, transparent), linear-gradient(to right, #26494a 50%, transparent 50%, transparent)',
    backgroundSize: '10px 10px',
  },
  head_title: { margin:' 5px 0 0 15px', float: 'left', fontSize: '14px', fontWeight: '600', },
  table: { background: '#ffffff' },
  head_row: {  height: '24px', },
  body_row: {  height: '12px', },
  foot_row: {  height: '24px', verticalAlign: 'bottom' },
  minus_value: { color: '#ff0000'},
  cell_brand_name: { minWidth: '70px', maxWidth: '100px' }, 
  cell_cfd_acc_name: { minWidth: '50px', maxWidth: '60px' }, 
  cell_sell_buy_div: { minWidth: '12px', maxWidth: '40px' }, 
  cell_last_contract_date: { minWidth: '105px', maxWidth: '120px' }, 
  cell_amount: { minWidth: '50px', maxWidth: '60px' }, 
  cell_ave_contract_rate: { minWidth: '50px', maxWidth: '70px' }, 
  cell_current_rate: { minWidth: '50px', maxWidth: '70px' }, 
  cell_pips: { minWidth: '30px', maxWidth: '70px' }, 
  cell_interest_adjustment: { minWidth: '100px', maxWidth: '110px' }, 
  cell_price_adjustment: { minWidth: '100px', maxWidth: '110px' }, 
  cell_profit: { minWidth: '100px', maxWidth: '110px' }, 
  cell_set_order: { minWidth: '90px', maxWidth: '100px' }, 
  set_order: { height: '12px', borderBottom: '1px solid #cccccc99', },
  set_order_type: { width:'50px', height: '12px', float: 'left', borderRight: '1px solid #cccccc99', },
  set_order_amount: { width:'50px', height: '12px', float: 'left', borderRight: '1px solid #cccccc99', },
  set_order_point: { width:'50px', height: '12px', float: 'left', borderRight: '1px solid #cccccc99', },
  set_order_reverse_point: { width:'50px', height: '12px', float: 'left', borderRight: '1px solid #cccccc99', },
  set_order_command: { width:'109px', height: '12px', float: 'left', },
  set_order_type_value: { width:'40px', },
  set_order_amount_value: { width:'40px', },
  set_order_point_value: { width:'40px', },
  set_order_reverse_point_value: { width:'40px', },
  set_order_command_value: { width:'100px', },
  set_order_button: { fontSize: '10px', height: '16px', marginTop: '-3px' }
});

export default function MainPositionSum(props) {
  const classes = useStyles();

  //PropsParam
  const selectedCfdAcc = props.selectedCfdAcc
  const selectedBrand = props.selectedBrand

  //Slice(Redux-State)
  const windowInfo = useSelector(state => getSliceVal(state, 'appTop', 'windowInfo'));
  const maxmin = useSelector(state => getSliceVal(state, 'rateView', 'maxmin'));
  const _positionSummaryList = useSelector(state => getSliceVal(state, 'position', 'positionSummaryList'));
  const positionSummaryList = getConditioningPositionList(_positionSummaryList, selectedCfdAcc, selectedBrand)
  const brandKeyMapList = useSelector(state => getSliceVal(state, 'brand', 'brandKeyMapList'));
  
  const totalProfit = calcTotalProfit(positionSummaryList)     //評価損益合計

  //描画値取得：現在値段
  const drawCurrRate = (row) => {
    return encDigit(row.currRate, row.brandCd, brandKeyMapList)
  }
  //描画値取得：pips
  const drawPips = (row) => {
    return encDigit(row.pips, row.brandCd, brandKeyMapList)
  }
  //描画値取得：評価損益
  const drawProfit = (row) => {
    if(row.profit) {
      return parseFloat(row.profit).toLocaleString() + ' 円'
    } else {
      return '0 円'
    }
  }
  //マイナス判定：pips
  const isMinusPips = (row) => {
    return row.pips < 0
  }
  //マイナス判定：評価損益
  const isMinusProfit = (row) => {
    return row.profit < 0
  }
  //マイナス判定：評価損益合計
  const isMinusTotalProfit = () => {
    return totalProfit < 0
  }

  return (
    <TableContainer className={classes.container} style={{ maxHeight: String(getMainContentsPartsViewHeight(maxmin, windowInfo))+'px' }}>
      <AppTable stickyHeader className={classes.table}>
        <TableHead>
          <TableRow className={classes.head_row}>
            <PositionTableCell align="center" className={classes.cell_brand_name}>銘柄</PositionTableCell>
            <PositionTableCell align="center" className={classes.cell_cfd_acc_name}>口座</PositionTableCell>
            <PositionTableCell align="center" className={classes.cell_sell_buy_div}>売買</PositionTableCell>
            <PositionTableCell align="center" className={classes.cell_last_contract_date}>最終成立日時</PositionTableCell>
            <PositionTableCell align="center" className={classes.cell_amount}>数量</PositionTableCell>
            <PositionTableCell align="center" className={classes.cell_ave_contract_rate}>平均値</PositionTableCell>
            <PositionTableCell align="center" className={classes.cell_current_rate}>現在値</PositionTableCell>
            <PositionTableCell align="center" className={classes.cell_pips}>pips</PositionTableCell>
            <PositionTableCell align="center" className={classes.cell_interest_adjustment}>金利調整額</PositionTableCell>
            <PositionTableCell align="center" className={classes.cell_price_adjustment}>価格調整額</PositionTableCell>
            <PositionTableCell align="center" className={classes.cell_profit}>評価損益</PositionTableCell>
            <PositionTableCell align="center" className={classes.cell_set_order}>決済</PositionTableCell>
          </TableRow>
        </TableHead>
        <TableBody>
        {positionSummaryList.map((row) => (
          <AppTableRow key={row.key} className={classes.body_row}>
            {/* 銘柄 */}
            <PositionTableCell align="center">{ getBrandName(row.brandCd, brandKeyMapList) }</PositionTableCell>
            {/* 口座 */}
            <PositionTableCell align="center">{ wrapGetCfdAccName(row.brandCd, brandKeyMapList) }</PositionTableCell>
            {/* 売買 */}
            <PositionTableCell align="center">{ getSellBuyName(row.sellBuyDiv) }</PositionTableCell>
            {/* 最終成立日時 */}
            <PositionTableCell align="center">{ encDispDate(row.lastContractDate) }</PositionTableCell>
            {/* 数量(万) */}
            <PositionTableCell align="right">{ getEncodedAmount(row.amount, row.brandCd, brandKeyMapList) }</PositionTableCell>
            {/* 平均値 */}
            <PositionTableCell align="right">{ encDigit(row.aveContractRate, row.brandCd, brandKeyMapList) }</PositionTableCell>
            {/* 現在値 */}
            <PositionTableCell align="right">{ drawCurrRate(row) }</PositionTableCell>
            {/* pips */}
            <PositionTableCell align="right" className={clsx({ [classes.minus_value]: isMinusPips(row), })}>
              { drawPips(row) }
            </PositionTableCell>
            {/* TODO 金利調整額 */}
            <PositionTableCell align="right" className={clsx({ [classes.minus_value]: isMinusProfit(row), })}>
              -999,999,999 円
            </PositionTableCell>
            {/* TODO 価格調整額 */}
            <PositionTableCell align="right" className={clsx({ [classes.minus_value]: isMinusProfit(row), })}>
             -999,999,999 円
            </PositionTableCell>
            {/* 評価損益 */}
            <PositionTableCell align="right" className={clsx({ [classes.minus_value]: isMinusProfit(row), })}>
              { drawProfit(row) }
            </PositionTableCell>
            {/* 一括決済 */}
            <PositionTableCell align="center">
              <OrderCommandButton  variant="contained" className={classes.set_order_button}>　　一括決済　　</OrderCommandButton>
            </PositionTableCell>
          </AppTableRow>
        ))}
        { positionSummaryList && positionSummaryList.length > 0 && (
          <AppTableFootRow className={classes.foot_row}>
            <PositionTableCell align="center" colSpan={3} style={{ borderRight:'none', }}></PositionTableCell>
            <PositionTableCell align="left" colSpan={5} style={{ borderRight:'none', }}>【合計】</PositionTableCell>
            <PositionTableCell align="right" style={{ borderRight:'none', }} className={clsx({ [classes.minus_value]: isMinusTotalProfit(), })}>
              { totalProfit.toLocaleString() } 円</PositionTableCell>
            <PositionTableCell align="right" style={{ borderRight:'none', }} className={clsx({ [classes.minus_value]: isMinusTotalProfit(), })}>
              { totalProfit.toLocaleString() } 円</PositionTableCell>
            <PositionTableCell align="right" style={{ borderRight:'none', }} className={clsx({ [classes.minus_value]: isMinusTotalProfit(), })}>
              { totalProfit.toLocaleString() } 円</PositionTableCell>
            <PositionTableCell align="center"></PositionTableCell>
          </AppTableFootRow>
        )}
        </TableBody>
      </AppTable>
    </TableContainer>
  );
}