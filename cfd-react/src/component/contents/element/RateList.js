import React from 'react';
import { useSelector } from 'react-redux'
import { makeStyles } from '@material-ui/core/styles';
import clsx from 'clsx';
import { useSpring } from 'react-spring'
import Paper from '@material-ui/core/Paper';
import TableContainer from '@material-ui/core/TableContainer';
import TableBody from '@material-ui/core/TableBody';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import { AppTable, AppTableRow, AppTableCell } from '../common/AppTable'
import { ChartIcon } from '../common/AppImageIcon'
import { getSliceVal } from '../../../utils/SystemUtils'
import { getBrandName, encDigit } from '../../../utils/BizzUtils'
import { isRateMaxDraw, isRateSmallMode, getRateBodyMaxHeight,
          RATE_BODY_FULL_HEIGHT, RATE_BODY_SMALL_HEIGHT, RATE_BODY_HIDE_HEIGHT } from '../../../utils/UiUtils'

const useStyles = makeStyles({
  root: { minWidth: '730px', maxWidth: '1014px', },
  container: { minWidth: '730px', maxWidth: '1014px', maxHeight: String(RATE_BODY_FULL_HEIGHT) + 'px', transition: 'all 300ms 0.1s ease', },
  container_small: { minWidth: '430px', maxWidth: '1014px', maxHeight: String(RATE_BODY_SMALL_HEIGHT) + 'px', transition: 'all 300ms 0.1s ease', },
  container_hide: { minWidth: '730px', maxWidth: '1014px', maxHeight: String(RATE_BODY_HIDE_HEIGHT) + 'px', transition: 'all 300ms 0s ease', },
  table: { minWidth: '430px', maxWidth: '1014px', },
  head_row: { height: '26px', },
  body_row: { height: '26px', },
  cell_brand_name: { minWidth: '100px', maxWidth: '150px', },
  cell_bid: { minWidth: '80px', maxWidth: '120px', },
  cell_offer: { minWidth: '80px', maxWidth: '120px', },
  cell_spread: { minWidth: '60px', maxWidth: '80px', },
  cell_start: { minWidth: '50px', maxWidth: '70px', },
  cell_high: { minWidth: '50px', maxWidth: '70px', },
  cell_low: { minWidth: '50px', maxWidth: '70px', },
  cell_compare_prev_day: { minWidth: '50px', maxWidth: '70px', },
  cell_chart: { minWidth: '60px', maxWidth: '70px', },
  cell_bid_value: { fontWeight: '700', fontSize: '16px', },
  cell_offer_value: { fontWeight: '700', fontSize: '16px', },
  cell_chart_icon: { paddingTop: '6px' },
});

export default function RateList(props) {
  const classes = useStyles();

  //PropsParam
  const currRateList = props.currRateList
  const brandKeyMapList = props.brandKeyMapList

  //Slice(Redux-State)
  const windowInfo = useSelector(state => getSliceVal(state, 'appTop', 'windowInfo'));
  const rateBodyMaxHeight = getRateBodyMaxHeight(windowInfo)
  const maxmin = useSelector(state => getSliceVal(state, 'rateView', 'maxmin'));

  const springRateList = useSpring({
    from: { maxHeight: isRateMaxDraw(maxmin) ? rateBodyMaxHeight : '0px', },    //※「0」の指定をするとうごかないので注意。「0px」で指定する必要あり。
    to: { maxHeight: isRateMaxDraw(maxmin) ? '0px': rateBodyMaxHeight, },
  });

  const isFullDraw = () => {
    if(!isRateMaxDraw(maxmin)) {
      return !isRateSmallMode(windowInfo)
    } else {
      return false
    }
  }

  const isSmallDraw = () => {
    if(!isRateMaxDraw(maxmin)) {
      return isRateSmallMode(windowInfo)
    } else {
      return false
    }
  }

  const isHideDraw = () => {
    return isRateMaxDraw(maxmin)
  }

  return (
    <Paper className={classes.root}>
      <TableContainer style={springRateList} className={clsx({ [classes.container]: isFullDraw(windowInfo), 
                                          [classes.container_small]: isSmallDraw(windowInfo),  [classes.container_hide]: isHideDraw(windowInfo), })}>
        <AppTable stickyHeader className={classes.table}>
          <TableHead>
            <TableRow className={classes.head_row}>
              <AppTableCell align="center" className={classes.cell_brand_name}>銘柄</AppTableCell>
              <AppTableCell align="center" className={classes.cell_bid}>売り</AppTableCell>
              <AppTableCell align="center" className={classes.cell_offer}>買い</AppTableCell>
              <AppTableCell align="center" className={classes.cell_spread}>スプレッド</AppTableCell>
              <AppTableCell align="center" className={classes.cell_start}>始値</AppTableCell>
              <AppTableCell align="center" className={classes.cell_high}>高値</AppTableCell>
              <AppTableCell align="center" className={classes.cell_low}>安値</AppTableCell>
              <AppTableCell align="center" className={classes.cell_compare_prev_day}>始値比</AppTableCell>
              <AppTableCell align="center" className={classes.cell_chart}>チャート</AppTableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {currRateList.map((row) => (
              <AppTableRow key={row.brandCd} className={classes.body_row}>
                <AppTableCell align="center">{getBrandName(row.brandCd, brandKeyMapList)}</AppTableCell>
                <AppTableCell align="right" className={classes.cell_bid_value}>
                  {encDigit(row.bid, row.brandCd, brandKeyMapList)}
                </AppTableCell>
                <AppTableCell align="right" className={classes.cell_offer_value}>
                  {encDigit(row.offer, row.brandCd, brandKeyMapList)}
                </AppTableCell>
                <AppTableCell align="right">{encDigit(row.spread, row.brandCd, brandKeyMapList)}</AppTableCell>
                <AppTableCell align="right">{encDigit(row.start, row.brandCd, brandKeyMapList)}</AppTableCell>
                <AppTableCell align="right">{encDigit(row.high, row.brandCd, brandKeyMapList)}</AppTableCell>
                <AppTableCell align="right">{encDigit(row.low, row.brandCd, brandKeyMapList)}</AppTableCell>
                <AppTableCell align="right">{encDigit(row.comparePrevDay, row.brandCd, brandKeyMapList)}</AppTableCell>
                <AppTableCell align="center" className={classes.cell_chart_icon}><ChartIcon/></AppTableCell>
              </AppTableRow>
            ))}
          </TableBody>
        </AppTable>
      </TableContainer>
    </Paper>
  );
}