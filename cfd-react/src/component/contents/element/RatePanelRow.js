import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';

const useStyles = makeStyles((theme) => ({
  panel: { width: '188px', height: '93px', border: '1px solid #4a898f66', textAlign: 'center', float: 'left', margin: '0 3px 2px 0',
           fontFamily: '"Hiragino Kaku Gothic ProN","メイリオ", sans-serif',  borderRadius: '9px',
           transition: 'transform 0.1s ease-out',
           '&:hover' : { transform: 'scale(1.04)', boxShadow: '5px 5px 10px 10px rgba(0,0,0,0.4);', },
         },
  panel_brand: { width: '100%', height: '19px', lineHeight: '19px', fontSize: '14px', borderBottom: '1px solid #4a898f66',
           background: '#4ea1a9ee', 
           backgroundImage: 'linear-gradient(to bottom, #4ea1a9ee 50%, transparent 50%, transparent), linear-gradient(to right, #4ea1a9ee 50%, transparent 50%, transparent)',
           backgroundSize: '5px 5px',
           color: '#ffffff', fontWeight: '500', borderRadius: '9px 9px 0 0' },
  panel_high_low_price: { width: '100%', height: '15px', lineHeight: '15px', borderBottom: '1px solid #4a898f66',
           background: '#5fc6d166', color: '#000000', fontWeight: '400' },
  panel_high_price: { width: '50%', float: 'left', fontSize: '11px', },
  panel_low_price: { width: '50%', float: 'left', fontSize: '11px', },
  panel_high_price_value: { marginLeft: '5px' },
  panel_low_price_value: { marginLeft: '5px' },
  panel_label_and_spread: { width: '100%', height: '17px', borderBottom: '1px solid #4a898f66', 
                            background: '#ffffff', color: '#000000', fontWeight: '400' },
  panel_label_bid: { width: 'calc(50% - 28px)', float: 'left', fontSize: '12px', },
  panel_spread: { width: '44px', float: 'left', fontSize: '12px', borderLeft: '1px solid #4a898f33', borderRight: '1px solid #4a898f33',
                padding: '0 5px', borderRadius: '6px', background: '#4a898f12' },
  panel_label_offer: { width: 'calc(50% - 28px)', float: 'left',  fontSize: '12px', },
  panel_current_price: { width: '100%', height: '38px', background: '#ffffff',fontWeight: '600', borderRadius: '0 0 9px 9px' },
  panel_current_price_bid: { width: 'calc(50% - 2px)', float: 'left', fontSize: '20px',  height: '38px', lineHeight: '48px', borderRight: '1px solid #4a898f66', 
           cursor: 'pointer', '&:hover': { backgroundColor: '#eeeeeeff'}, },
  panel_current_price_offer: { width: '50%', float: 'left', fontSize: '20px', height: '38px', lineHeight: '48px',
           cursor: 'pointer', '&:hover': { backgroundColor: '#eeeeeeff'}, },
}));

export default function RatePanelRecord(props) {

  //Hooks
  const classes = useStyles();

  //PropsParam
  const rate = props.rate
  const brand = props.brand

  //レート少数部桁数
  const dDigit = brand.rateDecimalDigit

  return (
    <Paper elevation={6} className={classes.panel}>
      <div className={classes.panel_brand}>{ brand.brandName }</div>
      <div className={classes.panel_high_low_price}>
        <div className={classes.panel_high_price}>高値<span className={ classes.panel_high_price_value }>{ rate.high.toFixed(dDigit) }</span></div>
        <div className={classes.panel_low_price}>安値<span className={ classes.panel_low_price_value }>{ rate.low.toFixed(dDigit) }</span></div>
      </div>
      <div className={classes.panel_label_and_spread}>
        <div className={classes.panel_label_bid}>売り</div>
        <div className={classes.panel_spread}>{ rate.spread.toFixed(dDigit) }</div>
        <div className={classes.panel_label_offer}>買い</div>
      </div>
      <div className={classes.panel_current_price}>
        <div className={classes.panel_current_price_bid}>{ rate.bid.toFixed(dDigit) }</div>
        <div className={classes.panel_current_price_offer}>{ rate.offer.toFixed(dDigit) }</div>
      </div>
    </Paper>
  );
}
