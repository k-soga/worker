import React from 'react';
import Modal from "react-modal";
import { useDispatch, useSelector } from 'react-redux';
import { makeStyles } from '@material-ui/core/styles';
import '../../../modal.css'
import NewOrderModalContents from './new_order/NewOrderModalContents'
import { getSliceVal } from '../../../utils/SystemUtils'
import { onChangeOpen } from '../../../store/ui/NewOrderModalSlice'

Modal.setAppElement("#root");
/*
const CONTENT_WIDTH = 560
const CONTENT_HEIGHT = 400

const modalStyle = {
  content: {
    position: "absolute",
    top: 0,
    left: 0,
    padding: 0,
    width: CONTENT_WIDTH + 'px',
    height: CONTENT_HEIGHT + 'px',
  }
};
*/
//https://www.happyhues.co/palettes/3
const useStyles = makeStyles((theme) => ({
  root: { width: '100%', height: '100%', },
}));

function AppModals() {
  //Hooks
  const classes = useStyles();
  const dispatch = useDispatch();

  //Slice(Redux-State)
  const isOpen = useSelector(state => getSliceVal(state, 'newOrderModal', 'isOpen'));

  const handleClickModal = () => {
    dispatch(onChangeOpen(!isOpen))
  }

  return (
    <div className={classes.root}>
      <Modal
        isOpen={isOpen}
        onRequestClose={() => handleClickModal(false)}
        overlayClassName={{
          base: "overlay-base",
          afterOpen: "overlay-after",
          beforeClose: "overlay-before"
        }}
        className={{
          base: "content-base",
          afterOpen: "content-after",
          beforeClose: "content-before"
        }}
        closeTimeoutMS={500}
      >
        <NewOrderModalContents/>
      </Modal>
      
    </div>
  );
}

export default AppModals;
