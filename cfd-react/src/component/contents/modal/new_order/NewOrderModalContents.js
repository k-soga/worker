import React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { makeStyles, withStyles, useTheme } from '@material-ui/core/styles';
import PropTypes from 'prop-types';
import SwipeableViews from 'react-swipeable-views';
import AppBar from '@material-ui/core/AppBar';
import Button from '@material-ui/core/Button';
import Paper from '@material-ui/core/Paper';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import Typography from '@material-ui/core/Typography';
import Box from '@material-ui/core/Box';
import IconButton from '@material-ui/core/IconButton';
import Close from '@material-ui/icons/Close';
import { AnchorButton } from '../../common/AppButtons'
import { getSliceVal } from '../../../../utils/SystemUtils'
import { onChangeOpen } from '../../../../store/ui/NewOrderModalSlice'

export const OrderButton = withStyles((theme) => ({
  root: {
    width: '200px',
    height: '40px',
    padding: '0',
    fontSize: '18px',
    fontWeight: '500',
    fontFamily: '"Hiragino Kaku Gothic ProN","メイリオ", sans-serif',
  },
}))(Button);

function TabPanel(props) {
  const { children, value, index, ...other } = props;
 return (
    <div
      role="tabpanel"
      hidden={value !== index}
      id={`full-width-tabpanel-${index}`}
      aria-labelledby={`full-width-tab-${index}`}
      {...other}
    >
      {value === index && (
        <Box p={3} style={{ padding: '1px' }} >
          <Typography>{children}</Typography>
        </Box>
      )}
    </div>
  );
}

TabPanel.propTypes = {
  children: PropTypes.node,
  index: PropTypes.any.isRequired,
  value: PropTypes.any.isRequired,
};

function a11yProps(index) {
  return {
    id: `full-width-tab-${index}`,
    'aria-controls': `full-width-tabpanel-${index}`,
  };
}


const useStyles = makeStyles((theme) => ({
  root: { width: '560px', height: '400px', },
  paper: { width: '100%', height: '100%', borderRadius: '10px', background: '#d2dde8' },
  head: { width: 'calc(500% -15px)', height: '30px', color: '#ffffff', margin: '3px 2px 5px 2px',
    backgroundColor: '#213e3b',
    backgroundImage: 'linear-gradient(to bottom, #26494a 50%, transparent 50%, transparent), linear-gradient(to right, #26494a 50%, transparent 50%, transparent)',
    backgroundSize: '10px 10px',
  },
  head_title: { margin:' 5px 0 0 15px', float: 'left', fontSize: '14px', fontWeight: '600', },
  input_area: { width:'556px', height:'330px', fontSize: '14px', background: '#ffffff', borderRadius: '0 0 10px 10px', margin: '1px' },
  column_row: { clear: 'both', borderBottom: '#d9d9d9 1px solid', height: '30px', lineHeight: '30px', },
  column_left: { float: 'left', borderRight: '#d9d9d9 1px solid', width : '150px', textAlign: 'center', background: '#5fc6d133' },
  column_righth: { float: 'left', paddingLeft: '15px'  },
  command_button_left: { float: 'left', width: '50%', margin: '0 -30px 0 30px' },
  command_button_right: { float: 'left', width: '50%', margin: '0 30px 0 -30px' },
  link_simulator: { float: 'right', margin: '0 15px 0 0' },
}));

function NewOrderModalContents() {
  //Hooks
  const classes = useStyles();
  const dispatch = useDispatch();

  //Slice(Redux-State)
  const isOpen = useSelector(state => getSliceVal(state, 'newOrderModal', 'isOpen'));

  const handleClickModal = () => {
    dispatch(onChangeOpen(!isOpen))
  }

  const theme = useTheme();
  const [value, setValue] = React.useState(0);

  const handleChange = (event, newValue) => {
    setValue(newValue);
  };

  const handleChangeIndex = (index) => {
    setValue(index);
  };

  const handleClickCancel = () => {
    dispatch(onChangeOpen(false))
  }

  return (
    <div className={classes.root}>
      <Paper className={classes.paper}>
        <Paper elevation={5} className={classes.head}>
          <div className={ classes.head_title }>新規注文</div>
          <div style={{ margin:'3px 10px 0 0 ', float: 'right', }}>
            <IconButton style={{ width: '14px', height: '14px', background: '#e0e0e0', cursor: 'pointer', borderRadius: ' 5px', padding: '11px', }} 
                        onClick={() => handleClickModal()}>
              <Close style={{ fontSize: '18px', color: '#000000', }} />
            </IconButton>
          </div>
        </Paper>
        <AppBar position="static" color="default">
          <Tabs style={{ minHeight: '30px', }} 
            value={value} onChange={handleChange}
            indicatorColor="primary" textColor="primary" variant="fullWidth" aria-label="full width tabs example"
          >
            <Tab label="成行" {...a11yProps(0)} style={{ minWidth: '140px', minHeight: '30px', padding:'0', fontSize: '14px' }} />
            <Tab label="指値" {...a11yProps(1)} style={{ minWidth: '140px', minHeight: '30px', padding:'0', fontSize: '14px' }} />
            <Tab label="逆指値" {...a11yProps(2)} style={{ minWidth: '140px', minHeight: '30px', padding:'0', fontSize: '14px' }} />
            <Tab label="イフダン" {...a11yProps(2)} style={{ minWidth: '140px', minHeight: '30px', padding:'0', fontSize: '14px' }} />
          </Tabs>
        </AppBar>
        <SwipeableViews axis={theme.direction === 'rtl' ? 'x-reverse' : 'x'}
          index={value} onChangeIndex={handleChangeIndex} >
          <TabPanel value={value} index={0} dir={theme.direction}>
            <div className={classes.input_area}>
              <div className={classes.column_row}>
                <div className={classes.column_left}>注文の種類</div>
                <div className={classes.column_righth}>成行(自動成立)</div>
              </div>
              <div className={classes.column_row}>
                <div className={classes.column_left}>銘柄</div>
                <div className={classes.column_righth}>日本225</div>
              </div>
              <div className={classes.column_row}>
                <div className={classes.column_left}>売買区分</div>
                <div className={classes.column_righth}>売</div>
              </div>
              <div className={classes.column_row} style={{ height: '60px', lineHeight: '60px' }}>
                <div className={classes.column_left}>取引数量</div>
                <div className={classes.column_righth}></div>
              </div>
              <div className={classes.column_row}>
                <div className={classes.column_left}>必要保証金</div>
                <div className={classes.column_righth}></div>
              </div>
              <div className={classes.column_row}>
                <div className={classes.column_left}>決済逆指値</div>
                <div className={classes.column_righth}></div>
              </div>
              <div className={classes.column_row}
               style={{width:'556px', height: '75px', lineHeight: '75px', margin: '0 auto', textAlign: 'center', borderBottom: '0' }}>
                <div className={classes.command_button_left}>
                  <OrderButton variant="contained" color="primary">注文の確認</OrderButton>
                </div>
                <div className={classes.command_button_right}>
                  <OrderButton variant="contained" onClick={() => handleClickCancel()}>キャンセル</OrderButton>
                </div>
              </div>
              <div className={classes.column_row}>
                <div className={classes.link_simulator}>
                  <AnchorButton>維持率シミュレーター</AnchorButton>
                </div>
              </div>
            </div>
          </TabPanel>
          <TabPanel value={value} index={1} dir={theme.direction}>
            <div className={classes.input_area}>
              <div className={classes.column_row}>
                <div className={classes.column_left}>注文の種類</div>
                <div className={classes.column_righth}>新規注文 - 指値</div>
              </div>
              <div className={classes.column_row}>
                <div className={classes.column_left}>銘柄</div>
                <div className={classes.column_righth}>日本225</div>
              </div>
              <div className={classes.column_row}>
                <div className={classes.column_left}>売買区分</div>
                <div className={classes.column_righth}>売</div>
              </div>
              <div className={classes.column_row} style={{ height: '60px', lineHeight: '60px' }}>
                <div className={classes.column_left}>取引数量</div>
                <div className={classes.column_righth}></div>
              </div>
              <div className={classes.column_row}>
                <div className={classes.column_left}>必要保証金</div>
                <div className={classes.column_righth}></div>
              </div>
              <div className={classes.column_row}>
                <div className={classes.column_left}>決済逆指値</div>
                <div className={classes.column_righth}></div>
              </div>
              <div className={classes.column_row}
               style={{width:'556px', height: '75px', lineHeight: '75px', margin: '0 auto', textAlign: 'center', borderBottom: '0' }}>
                <div className={classes.command_button_left}>
                  <OrderButton variant="contained" color="primary">注文の確認</OrderButton>
                </div>
                <div className={classes.command_button_right}>
                  <OrderButton variant="contained" onClick={() => handleClickCancel()}>キャンセル</OrderButton>
                </div>
              </div>
              <div className={classes.column_row}>
                <div className={classes.link_simulator}>
                  <AnchorButton>維持率シミュレーター</AnchorButton>
                </div>
              </div>
            </div>
          </TabPanel>
          <TabPanel value={value} index={2} dir={theme.direction}>
            <div className={classes.input_area}>
              <div className={classes.column_row}>
                <div className={classes.column_left}>注文の種類</div>
                <div className={classes.column_righth}>新規注文 - 逆指値</div>
              </div>
              <div className={classes.column_row}>
                <div className={classes.column_left}>銘柄</div>
                <div className={classes.column_righth}>日本225</div>
              </div>
              <div className={classes.column_row}>
                <div className={classes.column_left}>売買区分</div>
                <div className={classes.column_righth}>売</div>
              </div>
              <div className={classes.column_row} style={{ height: '60px', lineHeight: '60px' }}>
                <div className={classes.column_left}>取引数量</div>
                <div className={classes.column_righth}></div>
              </div>
              <div className={classes.column_row}>
                <div className={classes.column_left}>必要保証金</div>
                <div className={classes.column_righth}></div>
              </div>
              <div className={classes.column_row}>
                <div className={classes.column_left}>決済逆指値</div>
                <div className={classes.column_righth}></div>
              </div>
              <div className={classes.column_row}
               style={{width:'556px', height: '75px', lineHeight: '75px', margin: '0 auto', textAlign: 'center', borderBottom: '0' }}>
                <div className={classes.command_button_left}>
                  <OrderButton variant="contained" color="primary">注文の確認</OrderButton>
                </div>
                <div className={classes.command_button_right}>
                  <OrderButton variant="contained" onClick={() => handleClickCancel()}>キャンセル</OrderButton>
                </div>
              </div>
              <div className={classes.column_row}>
                <div className={classes.link_simulator}>
                  <AnchorButton>維持率シミュレーター</AnchorButton>
                </div>
              </div>
            </div>
          </TabPanel>
          <TabPanel value={value} index={3} dir={theme.direction}>
            <div className={classes.input_area}>
              <div className={classes.column_row}>
                <div className={classes.column_left}>注文の種類</div>
                <div className={classes.column_righth}>新規注文 - イフダン</div>
              </div>
              <div className={classes.column_row}>
                <div className={classes.column_left}>銘柄</div>
                <div className={classes.column_righth}>日本225</div>
              </div>
              <div className={classes.column_row}>
                <div className={classes.column_left}>売買区分</div>
                <div className={classes.column_righth}>売</div>
              </div>
              <div className={classes.column_row} style={{ height: '60px', lineHeight: '60px' }}>
                <div className={classes.column_left}>取引数量</div>
                <div className={classes.column_righth}></div>
              </div>
              <div className={classes.column_row}>
                <div className={classes.column_left}>必要保証金</div>
                <div className={classes.column_righth}></div>
              </div>
              <div className={classes.column_row}>
                <div className={classes.column_left}>決済逆指値</div>
                <div className={classes.column_righth}></div>
              </div>
              <div className={classes.column_row}
               style={{width:'556px', height: '75px', lineHeight: '75px', margin: '0 auto', textAlign: 'center', borderBottom: '0' }}>
                <div className={classes.command_button_left}>
                  <OrderButton variant="contained" color="primary">注文の確認</OrderButton>
                </div>
                <div className={classes.command_button_right}>
                  <OrderButton variant="contained" onClick={() => handleClickCancel()}>キャンセル</OrderButton>
                </div>
              </div>
              <div className={classes.column_row}>
                <div className={classes.link_simulator}>
                  <AnchorButton>維持率シミュレーター</AnchorButton>
                </div>
              </div>
            </div>
          </TabPanel>
        </SwipeableViews>
      </Paper>
    </div>
  );
}

export default NewOrderModalContents;
