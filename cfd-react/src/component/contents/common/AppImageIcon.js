import ChartIconImg from '../../../images/chart-icon.png'
import MaximizeIconImg from '../../../images/window_maximize_icon.png'
import MinimizeIconImg from '../../../images/window_minimize_icon.png'

export const ChartIcon = () =>{
  return <img src={ChartIconImg} alt="チャート" style={{ width: '24px', height: '12px', marginTop:'4px', cursor: 'pointer' }} />
}

export const MaximizeIcon = () =>{
  return <img src={MaximizeIconImg} alt="最大化" style={{ width: '22px', height: '22px', margin:'4px 4px 0 4px', cursor: 'pointer' }} />
}

export const MinimizeIcon = () =>{
  return <img src={MinimizeIconImg} alt="最小化" style={{ width: '22px', height: '22px', margin:'4px 4px 0 4px', cursor: 'pointer' }} />
}
