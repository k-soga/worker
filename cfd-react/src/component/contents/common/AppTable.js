import { withStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableRow from '@material-ui/core/TableRow';
import TableCell from '@material-ui/core/TableCell';

export const AppTable = withStyles((theme) => ({
  root: {
    background: 'none',
  },
}))(Table);

export const AppTableRow = withStyles((theme) => ({
  root: {
    '&:nth-of-type(odd)': {
      backgroundColor: 'rgba(0, 220, 220, 0.04)'
      // backgroundColor: theme.palette.action.hover,
    },
  },
}))(TableRow);

export const AppTableFootRow = withStyles((theme) => ({
  root: {
    backgroundColor: '#99c6d12c',
  },
}))(TableRow);

export const AppTableCell = withStyles((theme) => ({
  root: {
    padding: '0 5px',
    borderRight: '1px solid #cccccc99',
    fontSize: '13px',
    fontFamily: '"Hiragino Kaku Gothic ProN","メイリオ", sans-serif',
  },
  stickyHeader: {
    background: '#4ea1a9ee', 
    backgroundImage: 'linear-gradient(to bottom, #4ea1a9ee 50%, transparent 50%, transparent), linear-gradient(to right, #4ea1a9ee 50%, transparent 50%, transparent)',
    backgroundSize: '5px 5px',
    color: '#ffffff'
  },
}))(TableCell);

