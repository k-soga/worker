import { withStyles } from '@material-ui/core/styles';
import MenuItem from '@material-ui/core/MenuItem';
import Select from '@material-ui/core/Select';

export const AppSelect = withStyles((theme) => ({
  root: {
    fontSize: '12px',
    fontFamily: '"Hiragino Kaku Gothic ProN","メイリオ", sans-serif',
    background: '#ffffff',
    height: '20px',
    padding: '0 0 0 10px'
  },
  select: {
    fontSize: '12px',
    fontFamily: '"Hiragino Kaku Gothic ProN","メイリオ", sans-serif',
    background: '#ffffff',
    height: '20px',
    padding: '2px 0 0 10px',
    '&:focus': {
      background: '#ffffffdd',
    }

  },
}))(Select);

export const AppMenuItem = withStyles((theme) => ({
  root: {
    fontSize: '12px',
    fontFamily: '"Hiragino Kaku Gothic ProN","メイリオ", sans-serif',
    background: '#ffffff',
    height: '20px',
    padding: '2px 0 0 10px',
  },
}))(MenuItem);