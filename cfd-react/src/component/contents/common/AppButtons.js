import { withStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';

export const AnchorButton = withStyles((theme) => ({
  root: {
    padding: '0',
    fontSize: '14px',
    textDecoration: 'underline',
    color: '#0000ff',
    '&:hover': { textDecoration: 'underline', cursor: 'pointer', color: '#ff66cc', }
  },
}))(Button);
