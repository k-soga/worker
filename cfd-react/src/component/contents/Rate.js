import React from 'react';
import { useSelector, useDispatch } from 'react-redux'
import { makeStyles, withStyles } from '@material-ui/core/styles';
import clsx from 'clsx'
import { useSpring, animated } from 'react-spring'
import IconButton from '@material-ui/core/IconButton';
import Paper from '@material-ui/core/Paper';
import Tooltip from '@material-ui/core/Tooltip';
import RateList from './element/RateList'
import RatePanel from './element/RatePanel'
import Reorder from '@material-ui/icons/Reorder';
import ViewModule from '@material-ui/icons/ViewModule';
import Settings from '@material-ui/icons/Settings';
import VerticalAlignTop from '@material-ui/icons/VerticalAlignTop';
import VerticalAlignBottom from '@material-ui/icons/VerticalAlignBottom';
import ToggleButtonGroup from '@material-ui/lab/ToggleButtonGroup';
import RateToggleButton from './element/RateToggleButton'
import { MaximizeIcon, MinimizeIcon } from './common/AppImageIcon';
import { RATE_MODE, V_POSITION, MAXMIN_MODE,
         onChangeViewMode, onChangeVerticalPosition, onChangeMaxMinViewMode } from '../../store/ui/RateViewSlice'
import { getSliceVal } from '../../utils/SystemUtils'
import { isRateMaxDraw, getRateBodyMaxHeight } from '../../utils/UiUtils'

const RightSideIconButton = withStyles((theme) => ({
  root: {
    marginTop: '2px',
    padding: '0',
    color: '#ffffff',
  },
}))(IconButton);

const ProfitPaper = withStyles((theme) => ({
  root: {
    background: '#4ea1a9ee', 
    backgroundSize: '5px 5px', 
    backgroundImage: 'linear-gradient(to bottom, #4ea1a9ee 50%, transparent 50%, transparent), linear-gradient(to right, #4ea1a9ee 50%, transparent 50%, transparent)'
  },
}))(Paper);

const useStyles = makeStyles((theme) => ({
  root: { height: 'calc(100% - 3px)', background: '#d2dde8' },
  // head: { width: 'calc(100% - 15px)', height: '30px', color: '#ffffff', margin: '5px 2px 5px 0',
  head: { minWidth: '986px', height: '30px', color: '#ffffff', margin: '5px 2px 5px 0',
    backgroundColor: '#213e3b',
    backgroundImage: 'linear-gradient(to bottom, #26494a 50%, transparent 50%, transparent), linear-gradient(to right, #26494a 50%, transparent 50%, transparent)',
    backgroundSize: '10px 10px',
  },
  head_title: { margin:' 5px 0 0 15px', float: 'left', fontSize: '14px', fontWeight: '600', },
  btn_panel: { height: '12px', fontSize: '11px', margin: '5px 2px 5px 15px', padding: '10px 5px 10px 2px', },
  btn_list: { height: '12px', fontSize: '11px', margin: '5px 2px 5px 15px', padding: '10px 5px 10px 2px', },
  btn_setting: { height: '12px', fontSize: '12px', margin: '5px 0 5px', padding: '10px 5px 12px 5px',
                  background: '#ffffff', '&.MuiButton-startIcon': { marginRight: '2px', } },
  btn_max_min: { width: '20px', padding: '0' },
  rate_contents: { width: 'calc(100% - 2px)', marginRight: '2px', },
  rate_overFlowY: { overflowY: 'scroll', },
}));

export default function Rate() {

  //Hooks
  const classes = useStyles()
  const dispatch = useDispatch()

  //Slice(Redux-State)
  const windowInfo = useSelector(state => getSliceVal(state, 'appTop', 'windowInfo'));
  const viewMode = useSelector(state => getSliceVal(state, 'rateView', 'viewMode'));
  const vertical = useSelector(state => getSliceVal(state, 'rateView', 'vertical'));
  const maxmin = useSelector(state => getSliceVal(state, 'rateView', 'maxmin'));
  const currRateList = useSelector(state => getSliceVal(state, 'rate', 'currRateList'));
  const brandKeyMapList = useSelector(state => getSliceVal(state, 'brand', 'brandKeyMapList'));
  const rateBodyMaxHeight = getRateBodyMaxHeight(windowInfo)
  
  const springRateMaxMinPaper = useSpring({
    to: isRateMaxDraw(maxmin) ? { top: '32px', } : { top: '538px', },
    from: isRateMaxDraw(maxmin) ? { top: '538px', } : { top: '32px', }
  });

  const springRateMaxMin = useSpring({
    from: { height: isRateMaxDraw(maxmin) ? rateBodyMaxHeight : '0px', },    //※「0」の指定をするとうごかないので注意。「0px」で指定する必要あり。
    to: { height: isRateMaxDraw(maxmin) ? '0px': rateBodyMaxHeight, },
  });

  const handleClickViewMode = (event, newMode) => {
    if(newMode !== null && viewMode !== newMode) {
      dispatch(onChangeViewMode(newMode))
    }
  }

  const handleClickVerticalPosition = (type) => {
    dispatch(onChangeVerticalPosition(type))
  }

  const handleClickMaxMinViewMode = (type) => {
    dispatch(onChangeMaxMinViewMode(type))
  }

  const isListMode = () => {
    return viewMode === RATE_MODE.LIST
  }

  return (
    <Paper elevation={3} style={springRateMaxMinPaper} className={classes.root}>
      <div>
        <Paper elevation={5} className={classes.head}>
          <div style={{float: 'left', width: '920px'}}>
            <div className={ classes.head_title }>提供レート</div>
            <div style={{ marginLeft:'10px', float: 'left' }}>
              <ToggleButtonGroup value={viewMode} exclusive onChange={handleClickViewMode} aria-label="text alignment">
                <RateToggleButton value={RATE_MODE.PANEL} aria-label="left aligned" className={classes.btn_panel}>
                  <ViewModule />簡易
                </RateToggleButton>
                <RateToggleButton value={RATE_MODE.LIST} aria-label="centered" className={classes.btn_list}>
                  <Reorder />詳細
                </RateToggleButton>
              </ToggleButtonGroup>
            </div>
            <div style={{ margin:'3px 0 0 2px', float: 'left', }}>
              <Tooltip title="設定">
                <IconButton style={{ width: '16px', height: '16px', background: '#e0e0e0', cursor: 'pointer', borderRadius: ' 5px', padding: '11px', }}>
                  <Settings style={{ fontSize: '18px', color: '#000000', }} />
                </IconButton>
              </Tooltip>
            </div>
            <div style={{ margin:'3px 0 0 2px', float: 'left', }}>
              <ProfitPaper style={{ width: '330px', height: '22px', lineHeight: '22px', margin: '2px 0 0 2px', fontSize: '12px', }}>
                <div style={{ width: '24px', marginLeft: '5px', float: 'left', paddingRight: '7px', color: '#ffffff', fontWeight: '600' }}>
                  指数
                </div>
                <div style={{ background: '#ffffff', width: 'calc(100% - 50px)', marginTop: '3px', height: '16px', lineHeight: '16px', 
                  float: 'left', paddingRight: '5px', color: '#000000', fontWeight: '400', borderRadius: '7px', fontSize: '11px', }}>
                  <div style={{ width: '40px', float: 'left', margin: '0 0 0 8px', fontSize: '10px' }}>維持率：</div>
                  <div style={{ width: '46px', float: 'left', margin: '0 11px 0 8px', }}>
                    <span style={{ fontSize: '12px', fontWeight: '700' }}>000.00</span>&nbsp;%
                  </div>
                  <div style={{ float: 'left', color: '#aaaaaa', }}>|</div>
                  <div style={{ width: '50px', float: 'left', margin: '0 0 0 11px', fontSize: '10px' }}>評価損益：</div>
                  <div style={{ width: '100px', float: 'left', margin: '0 0 0 3px', }}>
                    <span style={{ fontSize: '12px', fontWeight: '700' }}>1,1111,111,111</span>&nbsp;円
                  </div>
                </div>
              </ProfitPaper>
            </div>
            <div style={{ margin:'3px 0 0 2px', float: 'left', }}>
              <ProfitPaper style={{ width: '330px', height: '22px', lineHeight: '22px', margin: '2px 0 0 2px', fontSize: '12px', }}>
                <div style={{ width: '24px', marginLeft: '5px', float: 'left', paddingRight: '7px', color: '#ffffff', fontWeight: '600' }}>
                  商品
                </div>
                <div style={{ background: '#ffffff', width: 'calc(100% - 50px)', marginTop: '3px', height: '16px', lineHeight: '16px', 
                  float: 'left', paddingRight: '5px', color: '#000000', fontWeight: '400', borderRadius: '7px', fontSize: '11px', }}>
                  <div style={{ width: '40px', float: 'left', margin: '0 0 0 8px', fontSize: '10px' }}>維持率：</div>
                  <div style={{ width: '46px', float: 'left', margin: '0 11px 0 8px', }}>
                    <span style={{ fontSize: '12px', fontWeight: '700' }}>000.00</span>&nbsp;%
                  </div>
                  <div style={{ float: 'left', color: '#aaaaaa', }}>|</div>
                  <div style={{ width: '50px', float: 'left', margin: '0 0 0 11px', fontSize: '10px' }}>評価損益：</div>
                  <div style={{ width: '100px', float: 'left', margin: '0 0 0 3px', }}>
                    <span style={{ fontSize: '12px', fontWeight: '700' }}>1,1111,111,111</span>&nbsp;円
                  </div>
                </div>
              </ProfitPaper>
            </div>
          </div>
          <div style={{float: 'right'}}>
            {(vertical !== V_POSITION.TOP) && (
              <RightSideIconButton onClick={() => handleClickVerticalPosition(V_POSITION.TOP)}>
                <VerticalAlignTop/>
              </RightSideIconButton>
            )}
            {(vertical === V_POSITION.TOP) && (
              <RightSideIconButton onClick={() => handleClickVerticalPosition(V_POSITION.BOTTOM)}>
                <VerticalAlignBottom/>
              </RightSideIconButton>
            )}
            {(maxmin === MAXMIN_MODE.MAX) && (
              <RightSideIconButton onClick={() => handleClickMaxMinViewMode(MAXMIN_MODE.MIN)}>
                <MaximizeIcon/>
              </RightSideIconButton>
            )}
            {(maxmin !== MAXMIN_MODE.MAX) && (
              <RightSideIconButton onClick={() => handleClickMaxMinViewMode(MAXMIN_MODE.MAX)}>
                <MinimizeIcon/>
              </RightSideIconButton>
            )}
          </div>
        </Paper>
      </div>
      <animated.div style={springRateMaxMin} className={clsx(classes.rate_contents, { [classes.rate_overFlowY]: !isListMode(), })}>
      {!isListMode() && ( 
        <RatePanel currRateList={currRateList} brandKeyMapList={brandKeyMapList} />
      )}
      {isListMode() && (
        <RateList currRateList={currRateList} brandKeyMapList={brandKeyMapList} />
      )}
      </animated.div>
    </Paper>
  );
}
