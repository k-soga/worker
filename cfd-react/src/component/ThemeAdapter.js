import React, { useState, useEffect } from 'react';
import { useSelector } from 'react-redux'
import { ThemeProvider } from '@material-ui/styles';
import { createMuiTheme } from '@material-ui/core/styles';
import App from './App'
import { getSliceVal } from '../utils/SystemUtils'
import { getSettingTheme } from '../theme/Themes'

function ThemeAdapter() {

  //Slice(Redux-State)  
  const colorPattern = useSelector(state => getSliceVal(state, 'uiSetting', 'colorPattern'));

  //Component-State
  const [isLoading, setLoading] = useState(true);

  useEffect(() => {
    if(isLoading) {
      setLoading(false)
    }
  // })
  },[isLoading])

  //テーマ取得
  const theme = createMuiTheme({
    appColors: getSettingTheme(colorPattern),
  });
  
  return (
    <ThemeProvider theme={theme}>
      <App/>
    </ThemeProvider>
  );
}

export default ThemeAdapter;
