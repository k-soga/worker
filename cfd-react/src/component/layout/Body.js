import React, { useRef } from 'react'
import { useSelector } from 'react-redux'
import { makeStyles } from '@material-ui/core/styles'
import { useSpring, useChain, animated } from 'react-spring'
import clsx from 'clsx';
import '../../anim.css'
import Rate from '../contents/Rate'
import Contents from './Contents'
import { getSliceVal } from '../../utils/SystemUtils'
import { MENU_CATEGORY_TYPES } from '../../const/Menus'
import { V_POSITION } from '../../store/ui/RateViewSlice'
import {
  isRateMaxDraw,
  isRateSmallMode,
  getRateViewMaxHeight,
  getFromTop4Rate,
  getToTop4Rate,
  getFromStyle4Contents,
  getToStyle4Contents,
} from '../../utils/UiUtils'

//https://www.happyhues.co/palettes/3
const useStyles = makeStyles((theme) => ({
  main_contents: { width: '100%', height: 'calc(100% - 30px);', position: 'absolute', zIndex: 10,
    boxShadow: '0px 2px 4px -1px rgb(0 0 0 / 20%), 0px 4px 5px 0px rgb(0 0 0 / 14%), 0px 1px 10px 0px rgb(0 0 0 / 12%)',
    transition: 'width 195ms cubic-bezier(0.4, 0, 0.6, 1) 0ms,margin 195ms cubic-bezier(0.4, 0, 0.6, 1) 0ms',
    margin: '30px auto 0',
  },
  rate_area: { position: 'fixed', zIndex: 15, width:'100%', margin: '0 auto'},
  contents_area: { position: 'fixed', width:'100%', margin: '-10px auto 0', },
  // rate:     { width: 'calc(100% - 10px)', minWidth:'986px', height: '240px', margin: '0 5px', },
  rate:     { width: 'calc(100% - 10px)', minWidth:'210px', height: '240px', margin: '0 5px', },
  contents: { width: 'calc(100% - 10px)', minWidth:'986px',  height: '100%', margin: '0 4px', },
  contents_scroll: { overflow: 'hidden' },
}));

export default function Body() {

  //Hooks
  const classes = useStyles();

  //Slice(Redux-State)
  const windowInfo = useSelector(state => getSliceVal(state, 'appTop', 'windowInfo'));
  const rateViewMaxHeight = getRateViewMaxHeight(windowInfo)
  const vertical = useSelector(state => getSliceVal(state, 'rateView', 'vertical'));
  const maxmin = useSelector(state => getSliceVal(state, 'rateView', 'maxmin'));
  const selectedMenuCategoryType = useSelector(state => getSliceVal(state, 'headMenu', 'selectedMenuCategoryType'));
  
  const isContentTypeMain = () => {
    return (!selectedMenuCategoryType) || selectedMenuCategoryType === MENU_CATEGORY_TYPES.MAIN
  }

  const isRateVerticalTop = () => {
    return vertical === V_POSITION.TOP
  }

  const springRateMaxMin = useSpring({
    to: isRateMaxDraw(maxmin) ? { height: '32px', } : { height: rateViewMaxHeight, },
    from: isRateMaxDraw(maxmin) ? { height: rateViewMaxHeight, } : { height: '42px', }
  });

  const springContentsMaxMin = useSpring({
    to: isRateMaxDraw(maxmin) ? { top: '538px', } : { top: '30px', },
    from: isRateMaxDraw(maxmin) ? { top: '30px', } : { top: '538px', }
  });

  const springRateRef = useRef()
  const getSpringRateStyleTo = () => {
    const isRateTop = isRateVerticalTop()
    const isRateMax = isRateMaxDraw(maxmin)
    const isRateSmall = isRateSmallMode(windowInfo)
    return getToTop4Rate(isRateTop, isRateMax, isRateSmall)
  }
  const getSpringRateStyleFrom = () => {
    const isRateTop = isRateVerticalTop()
    const isRateMax = isRateMaxDraw(maxmin)
    const isRateSmall = isRateSmallMode(windowInfo)
    return getFromTop4Rate(isRateTop, isRateMax, isRateSmall)
  }
  const springRate = useSpring({
    ref: springRateRef,
    to: getSpringRateStyleTo(),
    from: getSpringRateStyleFrom(),
  });
  /**
   * コンテンツ部 react-spring Style
   */
  const getSpringContentsStyleTo = () => {
    const isRateTop = isRateVerticalTop()
    const isRateMax = isRateMaxDraw(maxmin)
    const isRateSmall = isRateSmallMode(windowInfo)
    return getToStyle4Contents(isRateTop, isRateMax, isRateSmall)
  }
  const getSpringContentsStyleFrom = () => {
    const isRateTop = isRateVerticalTop()
    const isRateMax = isRateMaxDraw(maxmin)
    const isRateSmall = isRateSmallMode(windowInfo)
    return getFromStyle4Contents(isRateTop, isRateMax, isRateSmall)
  }
  const springContentsRef = useRef()
  const springContents = useSpring({
    ref: springContentsRef,
    to: getSpringContentsStyleTo(),
    from: getSpringContentsStyleFrom() 
  });

  useChain((vertical === V_POSITION.TOP) ? [springContentsRef, springRateRef] : [springRateRef, springContentsRef],
            (vertical === V_POSITION.TOP) ?  [0, 0.2] : [0.2, 0])

  return (
    <article className={classes.main_contents}>
      <animated.div style={springRate} className={clsx(classes.rate_area)}>
        <animated.div style={springRateMaxMin} className={ classes.rate }>
          <Rate/>
        </animated.div>
      </animated.div>
      <animated.div style={springContents} className={clsx(classes.contents_area)}>
        <animated.div style={springContentsMaxMin}
         className={clsx(classes.contents, { [classes.contents_scroll]: isContentTypeMain(), })}>
          <Contents/>
        </animated.div>
      </animated.div>
    </article>
  );
}
