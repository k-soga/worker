import React, { useState, useEffect } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import clsx from 'clsx';
import { makeStyles } from '@material-ui/core/styles';
import Divider from '@material-ui/core/Divider';
import Grid from '@material-ui/core/Grid';
import Menu from '@material-ui/core/Menu';
import MenuItem from '@material-ui/core/MenuItem';
import { getSliceVal } from '../../utils/SystemUtils'
import { onChangeSelected } from '../../store/ui/HeadMenuSlice'
import { onChangeOpen } from '../../store/ui/NewOrderModalSlice'
import { MENU_CATEGORY_TYPES } from '../../const/Menus'

//https://www.happyhues.co/palettes/3
const useStyles = makeStyles((theme) => ({
  header: { width: '100%', height: '30px;', lineHeight: '30px;', backgroundColor: '#5fc6d1', position: 'absolute',
    boxShadow: '0px 2px 4px -1px rgb(0 0 0 / 20%), 0px 4px 5px 0px rgb(0 0 0 / 14%), 0px 1px 10px 0px rgb(0 0 0 / 12%)',
    transition: 'width 195ms cubic-bezier(0.4, 0, 0.6, 1) 0ms,margin 195ms cubic-bezier(0.4, 0, 0.6, 1) 0ms',
    zIndex: 20, },
  // header_menu: { width: '986px', margin: '0 auto', },
  header_menu: { width:'100%', minWidth: '986px', margin: '0 auto', },
  gridRoot: { width: 'fit-content', color: '#ffffff', paddingTop: '3px', '& svg': { margin: theme.spacing(1.5), }, '& hr': { margin: theme.spacing(0, 0.5), }, },
  head_menu_btn: { color: '#ffffff', height: '25px', lineHeight: '25px', fontFamily: '"Hiragino Kaku Gothic ProN","メイリオ", sans-serif',
    fontSize: '13px', textDecoration: 'none', padding: '0 5px', borderRadius: '5px', cursor: 'pointer', 
    '&:hover': { color: '#000000', backgroundColor: '#ffffffdd', }, },
  head_menu_btn_selected: { color: '#000000', backgroundColor: '#ffffffee', '&:hover': { color: '#000000', backgroundColor: '#ffffffdd', }, },
  border: { backgroundColor: '#ffffff', height: '25px', lineHeight: '25px', marginTop: '2px', },
  sub_menu_area: { width: '986px', margin: '0 auto', },
  sub_menu_ref: { '& .MuiMenu-paper': { color: '#000000', backgroundColor: '#ffffffdd', }, },
  paper: { color: '#000000', backgroundColor: '#ffffffdd', },
  sub_menu_item_ref: { fontSize: '13px', },
}));


export default function HeadMenu() {
  //Hooks
  const classes = useStyles();
  const dispatch = useDispatch();

  //Slice(Redux-State)
  const selectedMenuType = useSelector(state => getSliceVal(state, 'headMenu', 'selectedMenuCategoryType'));
  const isOpenNewOrderModal = useSelector(state => getSliceVal(state, 'newOrderModal', 'isOpen'));

  //Component-State
  const [isLoading, setLoading] = useState(true);
  const [anchorEl, setAnchorEl] = useState(null);

  const handleClickRef = (event) => {
    setAnchorEl(event.currentTarget);
  };

  const handleCloseRef = () => {
    setAnchorEl(null);
  };

  useEffect(() => {
    if(isLoading) {
      setLoading(false)
    }
  },[isLoading, dispatch])

  const isSelected = (type) => {
    return selectedMenuType === type
  };

  const handleClick = (type) => {
    if(type !== MENU_CATEGORY_TYPES.NEW_ORDER && type !== MENU_CATEGORY_TYPES.REFERENCE) {
      dispatch(onChangeSelected(type))
    } else if(type === MENU_CATEGORY_TYPES.NEW_ORDER) {
      dispatch(onChangeOpen(!isOpenNewOrderModal))
    }
  }
 
  return (
    <header className={classes.header}>
      <div className={classes.header_menu}>
        <Grid container alignItems="center" className={classes.gridRoot}>
          <Divider orientation="vertical" flexItem className={classes.border} />
          <div className={clsx(classes.head_menu_btn, {[classes.head_menu_btn_selected]:isSelected(MENU_CATEGORY_TYPES.MAIN)})}
                  onClick={() => handleClick(MENU_CATEGORY_TYPES.MAIN)}>メイン画面</div>
          <Divider orientation="vertical" flexItem className={classes.border} />
          <div className={clsx(classes.head_menu_btn, {[classes.head_menu_btn_selected]:isSelected(MENU_CATEGORY_TYPES.NEW_ORDER)})}
                  onClick={() => handleClick(MENU_CATEGORY_TYPES.NEW_ORDER)}>新規注文</div>
          <Divider orientation="vertical" flexItem className={classes.border} />
          <div className={clsx(classes.head_menu_btn, {[classes.head_menu_btn_selected]:isSelected(MENU_CATEGORY_TYPES.SET_ORDER)})}
                  onClick={() => handleClick(MENU_CATEGORY_TYPES.SET_ORDER)}>決済注文</div>
          <Divider orientation="vertical" flexItem className={classes.border} />
          <div className={clsx(classes.head_menu_btn, {[classes.head_menu_btn_selected]:isSelected(MENU_CATEGORY_TYPES.CORRECT_ORDER)})}
                  onClick={() => handleClick(MENU_CATEGORY_TYPES.CORRECT_ORDER)}>注文訂正</div>
          <Divider orientation="vertical" flexItem className={classes.border} />
          <div className={clsx(classes.head_menu_btn, {[classes.head_menu_btn_selected]:isSelected(MENU_CATEGORY_TYPES.CANCEL_ORDER)})}
                  onClick={() => handleClick(MENU_CATEGORY_TYPES.CANCEL_ORDER)}>注文取消</div>
          <Divider orientation="vertical" flexItem className={classes.border} />
          <div className={clsx(classes.head_menu_btn, {[classes.head_menu_btn_selected]:isSelected(MENU_CATEGORY_TYPES.MULTI_SET_ORDER)})}
                  onClick={() => handleClick(MENU_CATEGORY_TYPES.MULTI_SET_ORDER)}>複数決済</div>
          <Divider orientation="vertical" flexItem className={classes.border} />
          <div className={clsx(classes.head_menu_btn, {[classes.head_menu_btn_selected]:isSelected(MENU_CATEGORY_TYPES.REFERENCE)})}
                  onClick={handleClickRef}>照会</div>
            <Menu id="simple-menu" anchorEl={anchorEl} keepMounted open={Boolean(anchorEl)} onClose={handleCloseRef} className={classes.sub_menu_ref}>
              <MenuItem onClick={handleCloseRef} className={classes.sub_menu_item_ref}>有効注文照会</MenuItem>
              <MenuItem onClick={handleCloseRef} className={classes.sub_menu_item_ref}>無効注文照会</MenuItem>
              <MenuItem onClick={handleCloseRef} className={classes.sub_menu_item_ref}>取引履歴照会</MenuItem>
              <MenuItem onClick={handleCloseRef} className={classes.sub_menu_item_ref}>ポジション照会</MenuItem>
              <MenuItem onClick={handleCloseRef} className={classes.sub_menu_item_ref}>代表口座照会</MenuItem>
            </Menu>
          <Divider orientation="vertical" flexItem className={classes.border} />
          <div className={clsx(classes.head_menu_btn, {[classes.head_menu_btn_selected]:isSelected(MENU_CATEGORY_TYPES.CLEAR_FORCED_SET_ORDER)})}
                  onClick={() => handleClick(MENU_CATEGORY_TYPES.CLEAR_FORCED_SET_ORDER)}>強制決済解消</div>
          <Divider orientation="vertical" flexItem className={classes.border} />
          <div className={clsx(classes.head_menu_btn, {[classes.head_menu_btn_selected]:isSelected(MENU_CATEGORY_TYPES.ORDER_SETTING)})}
                  onClick={() => handleClick(MENU_CATEGORY_TYPES.ORDER_SETTING)}>取引設定</div>
          <Divider orientation="vertical" flexItem className={classes.border} />
        </Grid>
      </div>
    </header>
  );
}
