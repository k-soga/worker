import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Main from '../contents/Main'

const useStyles = makeStyles((theme) => ({
  root: {
    height: 'calc(100% - 12px)',
  }
}));


export default function Contents() {
  const classes = useStyles();
  return (
    <div className={classes.root}>
      <Main />
    </div>
  );
}
