import { RUN_MODES, LOG_LEVELS } from '../../const/Env'

export const getApiEndpointUrl = (envRunMode) => {
  if(envRunMode === RUN_MODES.DEV) {
    return 'http://localhost:3000/'
  } else if(envRunMode === RUN_MODES.TEST) {
    return 'http://18.180.42.230/'
  } else {
    return 'http://18.180.42.230/'
  }
}

export const settingLogLevel = (envRunMode) => {
  if(envRunMode === RUN_MODES.DEV) {
    return LOG_LEVELS.TRACE
  } else if(envRunMode === RUN_MODES.TEST) {
    return LOG_LEVELS.INFO
  } else {
    return LOG_LEVELS.RUN
  }
}
