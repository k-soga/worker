import { LOG_LEVELS } from '../const/Env'
import { ENV_LOG_LEVEL } from '../env/Conf'

const _createlogger = (level) => {
  let _logger = {}
  _logger.debug = (msg) => {};
  _logger.info = (msg) => {};
  _logger.log = (msg) => {};
  _logger.warn = (msg) => {};
  _logger.error = (msg) => {};
  if(level >= LOG_LEVELS.TRACE) _logger.trace = (msg) => {console.trace(msg)};
  if(level >= LOG_LEVELS.DEBUG) _logger.debug = (msg) => {console.debug(msg)};
  if(level >= LOG_LEVELS.INFO) _logger.info = (msg) => {console.info(msg)};
  if(level >= LOG_LEVELS.LOG) _logger.log = (msg) => {console.log(msg)};
  if(level >= LOG_LEVELS.WARN) _logger.warn = (msg) => {console.warn(msg)};
  if(level >= LOG_LEVELS.ERROR) _logger.error = (msg) => {console.error(msg)};
  return _logger
};

const logger = _createlogger(ENV_LOG_LEVEL)

export default logger