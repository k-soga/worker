import { RUN_MODES } from '../const/Env'
import { getApiEndpointUrl, settingLogLevel } from './util/ConfUtil'

/********************************************************************
 * 稼働モード
 *   本番：PROD、テスト：TEST、開発：DEV
 ********************************************************************/
export const ENV_RUN_MODE = RUN_MODES.DEV

/********************************************************************
 * バージョン
 ********************************************************************/
 export const ENV_APP_VERSION = '0.0.1'

/********************************************************************
 * ログレベル
 ********************************************************************/
export const ENV_LOG_LEVEL = settingLogLevel(ENV_RUN_MODE)

/********************************************************************
 * ReduxStore設定
 ********************************************************************/
 export const ENV_REDUX_STORE_SLICE_PREFIX = 'fxp_cfd_pc_web'

 /********************************************************************
 * WebAPI設定
 ********************************************************************/
  export const ENV_WEBAPI_ENDPOINT = getApiEndpointUrl(ENV_RUN_MODE)

 